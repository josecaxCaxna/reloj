        <!--**********************************
            Footer start
        ***********************************-->
        <!-- <div class="footer">
            <div class="copyright">
                sdfsdfsdf
            </div>
        </div> -->
        <!--**********************************
            Footer end
        ***********************************-->

		<!--**********************************
           Support ticket button start
        ***********************************-->

        <!--**********************************
           Support ticket button end
        ***********************************-->


    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="<?php echo $rutaIndexArchivos; ?>/vendor/global/global.min.js"></script>
	<script src="<?php echo $rutaIndexArchivos; ?>/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/custom.min.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/dlabnav-init.js"></script>	
	
	<!-- Chart sparkline plugin files -->
    <script src="<?php echo $rutaIndexArchivos; ?>/vendor/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins-init/sparkline-init.js"></script>
	
	<!-- Chart Morris plugin files -->
    <script src="<?php echo $rutaIndexArchivos; ?>/vendor/raphael/raphael.min.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/vendor/morris/morris.min.js"></script> 
	
    <!-- Init file -->
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins-init/widgets-script-init.js"></script>
	
	<!-- Demo scripts -->
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/dashboard/dashboard.js"></script>
	
	<!-- Summernote -->
    <script src="<?php echo $rutaIndexArchivos; ?>/vendor/summernote/js/summernote.min.js"></script>
    <!-- Summernote init -->
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins-init/summernote-init.js"></script>
	
	<!-- Svganimation scripts -->
    <script src="<?php echo $rutaIndexArchivos; ?>/vendor/svganimation/vivus.min.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/vendor/svganimation/svg.animation.js"></script>
    <!-- <script src="<?php //echo $rutaIndexArchivos; ?>/public/js/styleSwitcher.js"></script> -->
	<script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins/sweetalert2/sweetalert2.all.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo $rutaIndexArchivos; ?>/public/js/plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
</body>
</html>