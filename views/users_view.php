<?php
// Verificar login
include(__DIR__.'/verifyLogin.php');

/**
* Dashboard de inicio
*/
  require_once(__DIR__.'/../controllers/users_controller.php');
  $claseIndex = new users_controller();
  $res = $claseIndex->listarUsuarios();
?>
<!-- Header -->
<?php include(__DIR__.'/head.php'); ?>
<!-- Header -->
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <!-- row -->
    <div class="container-fluid">				    
        <div class="row">					
            <div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Usuarios</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive recentOrderTable">
                            <table class="table verticle-middle table-responsive-md">
                                <thead>
                                    <tr>
                                        <th scope="col">Email</th>
                                        <th scope="col">Nombre</th>
                                        <th scope="col">Estatus</th>
                                        <th scope="col">Ultimo acceso</th>
                                        <th scope="col">Perfil</th>
                                        <th scope="col">Empresa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($res as $key => $value) {
                                        echo"
                                        <tr>
                                            <td>".$value['email']."</td>
                                            <td>".$value['nombre']."</td>
                                            <td>".($value['estatus'] == 0?'Inactivo':'Activo')."</td>
                                            <td>".$value['ultimo_acceso']."</td>
                                            <td>".($value['id_perfil'] == 0?'Super Admin':($value['id_perfil'] == 1?'Administrador':($value['id_perfil'] == 2?'Viewer':'N/A')))."</td>
                                            <td>".$value['nombre_empresa']."</td>
                                            <td>
                                                ".($value['id'] != $_SESSION['id_usuario']?"
                                                    <a onclick='editUser(".$value['id'].");' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a>":
                                                    "<a onclick='' disabled='disabled' class='btn btn-sm btn-primary' style='opacity: 0.65 !important;'><i class='la la-pencil'></i></a>")."                                                
                                                ".($value['id'] != $_SESSION['id_usuario']?"
                                                    <a onclick='editUser(".$value['id'].");' class='btn btn-sm btn-danger'><i class='la la-trash-o'></i></a>":
                                                    "<a onclick='' disabled='disabled' class='btn btn-sm btn-danger' style='opacity: 0.65 !important;'><i class='la la-trash-o'></i></a>")."
                                            </td>
                                        </tr>";
                                    } ?>                                                                        
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade bd-example-modal-lg" id="editUser" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Actualizar usuario</h4>
                        <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="basic-form">
                            <form>
                                <div class="form-row">                                    
                                    <div class="form-group col-md-6">
                                        <label>Nombre</label>
                                        <input type="text" id="nombreE" class="form-control" placeholder="Nombre">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Estatus</label>
                                        <select class="form-control form-control" id="estatusE">
                                            <option value="0">Inactivo</option>
                                            <option value="1">Activo</option>
                                        </select>  
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Contraseña</label>
                                        <input type="password" id="contraE" class="form-control" placeholder="Contraseña" autocomplete="off">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Perfil</label>
                                        <select class="form-control form-control" id="perfilE">
                                            <option value="0">Super Admin</option>
                                            <option value="1">Administrador</option>
                                            <option value="2">Viewer</option>
                                        </select>                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Guardar cambios</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function editUser(idUser){
        // console.log(idUser);
        // $('#btn-ingresar').click(function(){
        $.ajax({                        
            type: "POST",
            url: 'editarUsuario',
            dataType: 'json',
            data: {"type":"api","id_user":idUser},
            success: function(data){
                usuario = data[0];
                console.log(usuario);
                $('#nombreE').val(usuario.nombre);
                $('#estatusE').val(usuario.estatus).trigger('change');            
                $('#perfilE').val(usuario.id_perfil).trigger('change');

                $('#editUser').modal('show');
            }
        });
    }
</script>
<!--**********************************
    Content body end
***********************************-->
<!-- footer -->
<?php include(__DIR__.'/foot.php');  ?>
<!-- footer -->