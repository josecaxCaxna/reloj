<?php
    require_once(__DIR__.'/../models/index_model.php');
    /**
    * The home page controller
    */
    class horario_controller
    {
        function __construct()
        {
            $this->model = new horario_model();
        }

        public function index()
        {
            return $this->model->grupos();
        }
        public function grupos()
        {
            return $this->model->grupos();
        }
        
        public function horarios()
        {
            return $this->model->horarios();
        }
        
        public function asistencias()
        {
            return $this->model->asistencias();
        }

    }
    ?>