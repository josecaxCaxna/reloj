<?php
session_start();
$url = isset($_SERVER['PATH_INFO']) ? explode('/', ltrim($_SERVER['PATH_INFO'],'/')) : '/';
$ruta = explode('/',$_SERVER['PHP_SELF']);
$rutaIndex = "http://".$_SERVER['SERVER_NAME']."/".$ruta[1]."/index";
if ($url == '/'){
    $host= $_SERVER["HTTP_HOST"];
    $url= $_SERVER["REQUEST_URI"];
    $exp = explode(  "/", $url);
    $exps = $exp[1];
    $_SESSION['linkGlobal'] = "http://$host/$exps";
    if ( (isset($_COOKIE['token']) && isset($_COOKIE['usuario']) && isset($_SESSION['tokenUser'])) && ($_COOKIE['token'] == $_SESSION['tokenUser']) ) {
        header('Location: '.$rutaIndex.'');
    }
    require_once __DIR__.'/models/login_model.php';
    require_once __DIR__.'/controllers/login_controller.php';
    require_once __DIR__.'/views/login_view.php';
    $indexModel = New login_model();
    $indexController = New login_controller($indexModel);
    $indexView = New login_view($indexController, $indexModel);
    $indexView->index();
}else{
    // Nombre del controlador ej. index
    $requestedController = $url[0];
    // Accion de la ruta: show,edit,delete
    $requestedAction = isset($url[1])? $url[1] :'';
    // Parámetros de la ruta ej. id=0 , id=100, etc.
    $requestedParams = array_slice($url, 2);
    // Crea la ruta absoluta del archivo que debe estar alojado en el servidor ej. index_controller.php
    $ctrlPath = __DIR__.'\\controllers\\'.$requestedController.'_controller.php';                
    // Si el archivo existe en el servidor entra en el if
    if (file_exists($ctrlPath))
    {
        require_once __DIR__.'/models/'.$requestedController.'_model.php';
        require_once __DIR__.'/controllers/'.$requestedController.'_controller.php';                                       
        
        // Condicion para las peticiones por ajax, debe de llevar la bandera {"type":"api"} para que solo responda el json y no toda la vista
        if ( isset( $_POST['type'] ) && $_POST['type'] == 'api') {
            null;
        }else{
            require_once __DIR__.'/views/'.$requestedController.'_view.php';
        }        
        
        $modelName      = $requestedController.'_model';
        $controllerName = $requestedController.'_controller';
        $viewName       = $requestedController.'_view';
        // Instancia del controlador
        $controllerObj  = new $controllerName( new $modelName );
        // Instancia de la vista
        // $viewObj = new $viewName( $controllerObj, new $modelName );
        if ($requestedAction != '')
        {
            

            $metodos_clase = get_class_methods($controllerObj);
            if(in_array($requestedAction,$metodos_clase)){
                try {
                    $response = $controllerObj->$requestedAction($requestedParams);
                } catch (Exception $e) {
                    echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                    if ( (isset($_COOKIE['token']) && isset($_COOKIE['usuario']) && isset($_SESSION['tokenUser'])) && ($_COOKIE['token'] == $_SESSION['tokenUser']) ) {
                        echo"<script language='javascript'>window.location='".$_SESSION['linkGlobal']."/index'</script>;";
                        // header('Location: '.$_SESSION['linkGlobal']);                        
                    }else{
                        $_SESSION['message'] = 'Acceso no autorizado';
                        unset($_SESSION['tokenUser']);
                        header('Location: '.$_SESSION['linkGlobal']);
                    }
                }
            }else {
                if ( (isset($_COOKIE['token']) && isset($_COOKIE['usuario']) && isset($_SESSION['tokenUser'])) && ($_COOKIE['token'] == $_SESSION['tokenUser']) ) {
                    // echo"<script language='javascript'>window.location='/index'</script>;";
                    echo"<script language='javascript'>window.location='".$_SESSION['linkGlobal']."/index'</script>;";
                }else{
                    $_SESSION['message'] = 'Acceso no autorizado';
                    unset($_SESSION['tokenUser']);
                    header('Location: '.$_SESSION['linkGlobal']);
                }
            }

        }
    }else{ // Si el archivo no existe en el servidor, se retorna al index
        // Si el usuario se encuantra logueado y la cookie no ha dcaducado se le retorna al dashboard
        if ( (isset($_COOKIE['token']) && isset($_COOKIE['usuario']) && isset($_SESSION['tokenUser'])) && ($_COOKIE['token'] == $_SESSION['tokenUser']) ) {
            header('Location: '.$_SESSION['linkGlobal']);
        }else{
            // Se retorna al login con el error
            $_SESSION['message'] = 'Acceso no autorizado';
            unset($_SESSION['tokenUser']);
            header('Location: '.$_SESSION['linkGlobal']);
        }
    }
}
?>