<?php
    require_once(__DIR__.'/../config/dbConfig.php');
    /**
    * The home page model
    */
    class horario_model
    {
        protected $connection;
        function __construct()
        {
            $db = new dbConnection(); 
            $this->connection = $db->db();            
        }

        public function executeQuery($query){
            $resultado = $this->connection->query($query);
            if($resultado){
                $nFilas = $resultado->num_rows;
                if($nFilas > 0){
                    while($fila = $resultado->fetch_assoc())
                        $dataOut[] = $fila;
                    return $dataOut;
                }else
                    return ["data"=>"empty"];
            }else
                return $this->connection->error;
        }

        public function grupos(){
            $sql = "SELECT * FROM fp_horario_grupo";
            $resultado = $this->executeQuery($sql);            
            if ($resultado) {
                return $resultado;
            }            
        }
        
        public function horarios(){
            $sql = "SELECT * FROM fp_horario_grupo";
            $resultado = $this->executeQuery($sql);            
            if ($resultado) {
                return $resultado;
            }            
        }
        
        public function asistencias(){
            $sql = "SELECT * FROM fp_asistencia";
            $resultado = $this->executeQuery($sql);            
            if ($resultado) {
                return $resultado;
            }            
        }
    }
?>