// var curr = new Date(), y = curr.getFullYear(), m = curr.getMonth(), d = curr.getDate(); // get current date
// var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());
// var lastSunday = new Date(today.setDate(today.getDate()));
// console.log("lastSunday", lastSunday);

// var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
// var last = first + 7; // last day is the first day + 6
// var sundayCurr = y+'-'+('0' + (m + 1)).slice(-2)+'-'+last 
// console.log("sundayCurr", sundayCurr);

var isEmpty = ( input ) => ( input.length === 0 ) ? true : false ;
var idGrupo, idHorario = 0;
var empresa, empresaGrupoHorario, fechNomi, departamento, departamentoText = '';
$(document).ready(function(){
	
	var tblPersonRF = $('#tblPersonalRF').DataTable();
	// $(".loader").fadeOut("fast");
	
	// $('#fInicial, #fFinal').datepicker({
	//     format: 'dd-mm-yyyy',
	//     autoclose: true,
	//     language: 'es'
	// });

	// $('#fInicialHorasExtras, #fFinalHorasExtras').datepicker({
	//     format: 'dd-mm-yyyy',
	//     autoclose: true,
	//     language: 'es'
	// });
	
	$("#empresaGrupo").val('TOLUCA').selectpicker('refresh');
	$("#empresaHorarioEstablecido").val('TOLUCA').selectpicker('refresh');
	$("#empresaHorario").val('TOLUCA').selectpicker('refresh');
	$("#empresa").val('TOLUCA').selectpicker('refresh');
	$("#empresaReporte").val('TOLUCA').selectpicker('refresh');
	$("#empresaListaAsistencia").val('TOLUCA').selectpicker('refresh');
	$("#empresaHorasExtras").val('TOLUCA').selectpicker('refresh');
	//Timepicker
    
    $('.timepicker').timepicker({
        showInputs: false,
        showMeridian: false,
        minuteStep: 1
    });

    //Default Table Personal
    let Datos = {};
    Datos['empresa'] = 'TOLUCA';
    Datos['fechNomi'] = $("#fechaNominaGrupo option:selected").val();

    cargarTblPersonal(Datos);
    Datos['fechNomi'] = $("#fechaNominaGrupoEstablecido option:selected").val();
    cargarTblGrupos(Datos);
    Datos['fechNomi'] = $("#fechaNominaHorarioEstablecido option:selected").val();
    cargarTblHorarios(Datos);
    Datos['fechNomi'] = $("#fechaNomina option:selected").val();
    cargarTblGrupoHorario(Datos);
    // Datos['fechNomi'] = $("#fechaNominaPD option:selected").val();
    // cargartblPersonalPrimaDominical(Datos);
    // Datos['fechNomi'] = $("#fechaNominaPP option:selected").val();
    // cargartblPersonalPremioPuntualidad(Datos);
    // cargarGrupoHorarioAsignado(Datos);
    refreschSelectUbicacion();

    $('.nav-tabs a').on('shown.bs.tab', function(event){

    	var referencia = $(event.target).attr("href");
    	// console.log("referencia", referencia);
       
        if(referencia == "#mod-primaDominical")
        {
            $("#tblPersonalPD").DataTable({
				searching : false,
				paging : false,
		        scrollY: '60vh',
		      	scrollCollapse: true,
		        // order: [[ 1, "asc" ], [ 3, 'asc' ]],
	        	retrieve: true
       		}).columns.adjust();
        }
        if(referencia == "#mod-premioPuntualidad")
        {
        	$("#tblPersonalPP").DataTable({
				searching : false,
				paging : false,
		        scrollY: '60vh',
		        scrollX: true,
		      	scrollCollapse: true,
		        order: [[ 1, "asc" ], [ 3, 'asc' ]],
	        	retrieve: true
       		}).columns.adjust();
        }
        if(referencia == "#registro-horario")
        {
        	$("#tblHorarios").DataTable({
				searching : false,
				paging : false,
				order: [[ 0, "asc" ]],
	            scrollY: '60vh',
	      		scrollCollapse: true,
          		retrieve: true
       		}).columns.adjust();
        }

        if(referencia == "#mod-faltas")
        {
        	$("#tblPersonalRF").DataTable({
				order: [[ 0, "asc" ]],
	            scrollY: '60vh',
	      		scrollCollapse: true,
          		retrieve: true
       		}).columns.adjust();
        }
    });

   //  $(".loading").fadeIn("slow", function()
  	// {
   //  	setTimeout(function(){ $(".loading").fadeOut("fast"); }, 20000);
   //  });

 //   $(document).keydown(function (event) {
	//     if (event.keyCode == 123) { // Prevent F12
	//         return false;
	//     } 
	//     else if (event.ctrlKey && event.shiftKey && event.keyCode == 73) { // Prevent Ctrl+Shift+I        
	//         return false;
 //    	}
	// });

	// $(document).on("contextmenu", function (e) {        
	//     e.preventDefault();
	// });

    $(".loading").fadeOut("fast");
    
});

// function iniciarDataTable(id){
//     var table = $('#'+id).DataTable({
//       //todo el codigo
//     	searching : false,
// 		paging : false,
//         scrollY: '60vh',
//       	scrollCollapse: true,
//         order: [[ 1, "asc" ], [ 3, 'asc' ]],
//         retrieve: true
//     });

//     // table.buttons().container().appendTo( '#example_wrapper .col-md-6:eq(0)');

//     return table;
// } 

$(document).on("change", "#empresaGrupo", function()
{
	let empresa = $(this).val();
	if(!isEmpty(empresa))
		cargarPeriodoNominaGrupo(empresa)

});

function cargarPeriodoNominaGrupo(empresa)
{
	let Datos = {};
	Datos['empresa'] = empresa;
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominal',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
    				$("#fechaNominaGrupo").find('option').remove().end().append(option);
    				Datos['fechNomi'] = $("#fechaNominaGrupo option:selected").val();
  					$('#fechaNominaGrupo').val(Datos['fechNomi']).selectpicker('refresh');
  					cargarTblPersonal(Datos);
        		}
        		else
        		{
    				$("#fechaNominaGrupo").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaGrupo').val(null).selectpicker('refresh');
    				cleanTblPersonal()
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

function cleanTblPersonal()
{
	$("#tblPersonal").DataTable().clear().destroy();
    $("#tblPersonal tbody").html('');
	$("#tblPersonal").DataTable({
		searching : false,
		paging : false,
        scrollY: '60vh',
      	scrollCollapse: true,
        order: [[ 1, "asc" ], [ 3, 'asc' ]],
        retrieve: true
   	});
}

$(document).on("change", "#fechaNominaGrupo", function()
{
		let Datos = {};
		Datos['empresa'] = $("#empresaGrupo option:selected").val();
		Datos['fechNomi'] = $(this).val(); 
		cargarTblPersonal(Datos);

});

function cargarTblPersonal(Datos)
{
	$(".loader").fadeIn("slow", function()
  	{
	    $.ajax(
	    {
		    data:{ datos:Datos },
	        url: 'crudHorario.php?accion=tblPersonal',
	        type: 'post',
	        success: function (tr) 
	        {
		        $("#tblPersonal").DataTable().clear().destroy();
		        $("#tblPersonal tbody").html(tr);
				$("#tblPersonal").DataTable({
					searching : false,
					paging : false,
			        scrollY: '60vh',
			      	scrollCollapse: true,
			        order: [[ 1, "asc" ], [ 3, 'asc' ]],
			        retrieve: true
		       	});

          		$(".loader").fadeOut("fast");

        	}
	    });
	});
}

$(document).on("keyup change", "input#nombreGrupo", function()
{
	let nombreGrupo = $('#nombreGrupo').val();
	let countCheck = $("input.checkPersonal:checked").length;

	$("#btnCrearGrupo").prop("disabled", true);
	if(!isEmpty(nombreGrupo) && countCheck > 0)
			$("#btnCrearGrupo").prop("disabled", false);
});

$(document).on("click", "input.checkPersonal", function(){

		let nombreGrupo = $('#nombreGrupo').val();
		let countCheck = $("input.checkPersonal:checked").length;

		$("#btnCrearGrupo").prop("disabled", true);
		if(!isEmpty(nombreGrupo) && countCheck > 0)
				$("#btnCrearGrupo").prop("disabled", false);

});


$(document).on("click","#btnCrearGrupo", function()
{
	let Datos = {};

	Datos['empresa'] = $('#empresaGrupo option:selected').val();
	Datos['fechNomi'] = $('#fechaNominaGrupo option:selected').val();
	Datos['nombreGrupo'] = $('#nombreGrupo').val();
	Datos['cve_trabajador'] = [];

  	$("#tblPersonal > tbody > tr").each(function(){

  		let checkNum = $(this).find('td:eq(0) input[type="checkbox"]');

  		if(checkNum.is(':checked'))
  		{
  			Datos['cve_trabajador'].push( $(this).find("td:eq(2)").text() );
  		}

  	});

	if(!isEmpty(Datos['empresa']) && !isEmpty(Datos['nombreGrupo']) && Datos['cve_trabajador'].length > 0)
		guardarGrupo(Datos);
	

});

function guardarGrupo(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
    	$.ajax(
    	{
    		data:{ datos:Datos },
        	url: 'crudHorario.php?accion=guardarGrupo',
        	type: 'post',
        	success: function (data) 
        	{
        		clearModuloGrupos()

            	if(data.trim() == '0')
            	{
	                swal(
	                    '¡No fue posible agregar el registro!',
	                    'Volver a intentar nuevamente',
	                    'warning'
	                )

            	}
            	else if(data.trim() == '1')
            	{

                	cargarTblPersonal(Datos);
                	updateGrupoEstablecido(Datos);
                	updateModAsignacionHorario(Datos);

                	swal(
	                    '¡Alta de registro!',
	                    'Se agrego correctamente',
	                    'success'
	               	)
	               	
	            }
	            else
	            {
		            swal(
	                    'No es posible realizar alta de registro',
	                    data.trim(),
	                    'warning'
	                )
	                
	            }
	            $(".loader").fadeOut("fast");

	            
        	}
    	});
	});
}

function updateGrupoEstablecido(Datos)
{
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:Datos['empresa']},
	      	url: 'crudHorario.php?accion=periodoNominalGrupoEstablecido',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
        			$("#empresaGrupoEstablecido").val(Datos['empresa']).selectpicker("refresh");
    				$("#fechaNominaGrupoEstablecido").find('option').remove().end().append(option);
      				$('#fechaNominaGrupoEstablecido').val(Datos['fechNomi']).selectpicker('refresh');
      				cargarTblGrupos(Datos);
        		}
        		else
        		{
    				$("#fechaNominaGrupoEstablecido").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaGrupoEstablecido').val(null).selectpicker('refresh');
    				cleanTblGrupos()
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}
// function selectFechNomiGrupoEstablecido()
// {
// 	$(".loader").fadeIn("slow", function(){

// 		$.ajax(
// 	    {
// 	        url: 'crudHorario.php?accion=selectFechNomiGrupoEstablecido',
// 	        type: 'post',
// 	        success: function (option) 
// 	        {
// 	            $(".loader").fadeOut("fast");
	            
// 	            $("#fechaNominaGrupoHorario").find('option').remove().end().append(option);
// 	            // $("#editAddress").append(option);
	            
// 	            $('#fechaNominaGrupoHorario').selectpicker('refresh');
// 	            // $('#editAddress').selectpicker('refresh');
// 	        }
// 	    });

// 	});
// }

$(document).on("click", "#btnCancelarGrupo", function()
{
		clearModuloGrupos()
});

function clearModuloGrupos()
{
	$("#btnCrearGrupo").prop("disabled", true);
	$("#nombreGrupo").val(null);
	$("input.checkPersonal").prop("checked", false);
}

$(document).on("change", "#empresaGrupoEstablecido", function()
{
	let empresa = $(this).val();
	if(!isEmpty(empresa))
		cargarPeriodoNominaGrupoEstablecido(empresa);

});

function cargarPeriodoNominaGrupoEstablecido(empresa)
{
	let Datos = {};
	Datos['empresa'] = empresa;
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominalGrupoEstablecido',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
    				$("#fechaNominaGrupoEstablecido").find('option').remove().end().append(option);
    				Datos['fechNomi'] = $("#fechaNominaGrupoEstablecido option:selected").val();
      				$('#fechaNominaGrupoEstablecido').val(Datos['fechNomi']).selectpicker('refresh');
      				cargarTblGrupos(Datos);
        		}
        		else
        		{
    				$("#fechaNominaGrupoEstablecido").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaGrupoEstablecido').val(null).selectpicker('refresh');
    				cleanTblGrupos()
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

$(document).on("change", "#fechaNominaGrupoEstablecido", function()
{
	let Datos = {};

	Datos['empresa'] = $("#empresaGrupoEstablecido option:selected").val();
	Datos['fechNomi'] = $("#fechaNominaGrupoEstablecido option:selected").val();

	if(!isEmpty(Datos['empresa']) && !isEmpty(Datos['fechNomi']))
		cargarTblGrupos(Datos);

});

function cargarTblGrupos(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
  		$.ajax(
    	{
    		data:{ datos:Datos },
      		url: 'crudHorario.php?accion=tblGrupos',
        	type: 'post',
        	success: function (tr) 
        	{
        		let countOption = $('#fechaNominaGrupoEstablecido option'),length;
    			$('#empresaGrupoEstablecido').val(Datos['empresa']).selectpicker('refresh');
    			if(Datos['fechNomi'] != undefined && countOption > 0)
    				$('#fechaNominaGrupoEstablecido').val(Datos['fechNomi']).selectpicker('refresh');

    			$("#tblGrupos").DataTable().clear().destroy();
      			$("#tblGrupos tbody").html(tr);
				$("#tblGrupos").DataTable({
              		searching : false,
					paging : false,
		            scrollY: '60vh',
		      		scrollCollapse: true,
              		retrieve: true
          		});

      			$(".loader").fadeOut("fast");

        	}
    	});
	});
}

function cleanTblGrupos()
{
	$("#tblGrupos").DataTable().clear().destroy();
	$("#tblGrupos tbody").html(null);
	$("#tblGrupos").DataTable({
  		searching : false,
		paging : false,
        scrollY: '60vh',
  		scrollCollapse: true,
  		retrieve: true
	});
}


$(document).on("click", "button.btnDetalleGrupo", function(){

	let fila = $(this).closest("tr");
	let grupoEstablecido = fila.find('td:eq(1)').text();
	$("#viewGrupo").text("Trabajadores del grupo: "+grupoEstablecido);
	idGrupo = $(this).attr("idGrupo");
	cargarDetalleGrupo(idGrupo);

});

function cargarDetalleGrupo(idGrupo)
{
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
		{
			data: { idGrupo:idGrupo },
  			url: 'crudHorario.php?accion=cargarDetalleGrupo',
  			type: 'post',
  			success: function (tr) 
  			{
				$("#tblGrupoTrabajadores").DataTable().clear().destroy();
      			$("#tblGrupoTrabajadores tbody").html(tr);
				$("#tblGrupoTrabajadores").DataTable({
              		searching : false,
					paging : false,
					order: [[ 0, "asc" ], [ 1, 'asc' ]]
          		});

          		$("#modal_grupo_trabajador").modal("show");

      			$(".loader").fadeOut("fast");
  			}
  		});
    });
}

$(document).on("click", "button.btnEliminarTrabajador", function()
{
	let DatosGH = {};
	DatosGH['empresa'] = $('#empresaGrupo option:selected').val();
	DatosGH['fechNomi'] = $('#fechaNominaGrupo option:selected').val(); 

	let cve_delete = $(this).attr("cveTrabajadorGrupo");

	let Datos = {};
	Datos['idGrupo'] = idGrupo;
	Datos['cve_trabajadores'] = [];

	$("#tblGrupoTrabajadores > tbody > tr").each(function(){

		let cve_trabajador = $(this).find("td:eq(1)").text();
		if(cve_delete != cve_trabajador)
				Datos['cve_trabajadores'].push( cve_trabajador );
	});

	// console.log("Datos['cve_trabajadores']", Datos['cve_trabajadores']);

	swal({
		title: '¿Está seguro de eliminar el trabajador del grupo?',
		text: "¡Si no lo está puede cancelar la accíón!",
		type: 'warning',
	    showCancelButton: true,
	    confirmButtonColor: '#3085d6',
	    cancelButtonColor: '#d33',
	    cancelButtonText: 'Cancelar',
	    confirmButtonText: 'Si, eliminar trabajador!'
	}).then(function(result){

		if(result.value){
			deleteTrabajadorGrupo(DatosGH, Datos);
		}

	});
		
});

function deleteTrabajadorGrupo(DatosGH, Datos)
{
	$(".loader").fadeIn("slow", function()
    {
    	$.ajax(
    	{
    		data:{ datos:Datos },
        	url: 'crudHorario.php?accion=deleteTrabajadorGrupo',
        	type: 'post',
        	success: function (data) 
        	{
        			
            	if(data.trim() == '0')
            	{
	                swal(
	                    '¡No fue posible eliminar el registro!',
	                    'Volver a intentar nuevamente',
	                    'warning'
	                )
            	}
            	else if(data.trim() == '1')
            	{
	            	cargarDetalleGrupo(idGrupo);
	            	cargarTblPersonal(DatosGH);
	            }
	            else
	            {
		            swal(
	                    '¡No es posible eliminar el registro!',
	                    data,
	                    'warning'
	                )
	            }

	            
	            $(".loader").fadeOut("fast");
        	}
    	});
	});
}

$(document).on("click", "button.btnUserGrupo", function()
{
		let fila = $(this).closest("tr");
		let grupoEstablecido = fila.find('td:eq(1)').text();
		$("#addUser").text("Agregar usuario al grupo: "+grupoEstablecido);
		idGrupo = $(this).attr("idGrupo");
		cargarTblUsuarios(idGrupo);
});

function cargarTblUsuarios(idGrupo)
{
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
		{
			data: { idGrupo:idGrupo },
  			url: 'crudHorario.php?accion=cargarTblUsuarios',
  			type: 'post',
  			success: function (tr) 
  			{
  				$("#tblGrupoUsuarios").DataTable().clear().destroy();
          		$("#tblGrupoUsuarios tbody").html(tr);
				$("#tblGrupoUsuarios").DataTable({
	              	searching : false,
					paging : false,
					order: [[ 1, "asc" ]]
	          	});

	          	$("#modal_grupo_usuarios").modal("show");

          		$(".loader").fadeOut("fast");
  			}
  		});
    });
}

$(document).on("click", "input.checkUser", function(){

		let countCheck = $("input.checkUser:checked").length;

		$("#btnGuardarUsuario").prop("disabled", true);
		if(countCheck > 0)
				$("#btnGuardarUsuario").prop("disabled", false);

});

$(document).on("click", "#btnGuardarUsuario", function()
{
	let Datos = {};
	let countCheck = $("input.checkUser:checked").length;

	Datos['idGrupo'] = idGrupo;
	Datos['usuarios'] = [];

  	$("#tblGrupoUsuarios > tbody > tr").each(function(){

  			let checkNum = $(this).find('td:eq(0) input[type="checkbox"]');

  			if(checkNum.is(':checked'))
  			{
  				Datos['usuarios'].push( $(this).find("td:eq(1)").text() );
  			}

  	});
		
	if(countCheck > 0 && idGrupo > 0 && Datos['usuarios'].length > 0)
		guardarAccesoUser(Datos);
});

function guardarAccesoUser(Datos)
{

	$(".loader").fadeIn("slow", function()
    {
  		$.ajax(
    	{
    		data:{ datos:Datos },
      		url: 'crudHorario.php?accion=guardarAccesoUser',
        	type: 'post',
        	success: function (data) 
        	{
          		if(data.trim() == '0')
            	{
	                swal(
	                    '¡No fue posible agregar el registro!',
	                    'Volver a intentar nuevamente',
	                    'warning'
	                )
            	}else
            	if(data.trim() == '1')
            	{
            		$("#btnGuardarUsuario").prop("disabled", true);
		            swal(
	                    '¡Alta de registro!',
	                    'Se agrego correctamente',
	                    'success'
	                )

            			// clearFields()
               			// cargarTblHorarios()
	            }
	            else
	            {
	            	swal(
	                    '¡No se puede realizar alta de registro!',
	                    data,
	                    'warning'
	                )
	            }
            	$(".loader").fadeOut("fast");

        	}
    	});
	});
}

$(document).on("click", "button.btnAddTrabajadorGrupo", function(){

	let fila = $(this).closest("tr");
	let grupoEstablecido = fila.find('td:eq(1)').text();

	$("#addWorker").text("Agregar trabajador al grupo: "+grupoEstablecido);

	idGrupo = $(this).attr("idGrupo");
	trabajadoresGrupoEstablecido(idGrupo);

});

function trabajadoresGrupoEstablecido(idGrupo)
{
	$(".loader").fadeIn("slow", function()
  	{
    		$.ajax(
    		{
    			data: { idGrupo:idGrupo },
      			url: 'crudHorario.php?accion=cargarTblGrupoEstablecido',
      			type: 'post',
      			success: function (tr) 
      			{
      				$("#tblGrupoEstablecido").DataTable().clear().destroy();
	          		$("#tblGrupoEstablecido tbody").html(tr);
					$("#tblGrupoEstablecido").DataTable({
		              	searching : false,
						paging : false,
						order: [[ 1, "asc" ]]
		          	});

		          	$("#modal_grupoEstablecido").modal('show');

	          		$(".loader").fadeOut("fast");
      			}
      	});
    });
		
}

$(document).on("click", "input.checkPersonalGrupo", function(){

	let countCheck = $("input.checkPersonalGrupo:checked").length;

	$("#btnGuardarGrupo").prop("disabled", true);
	if(countCheck > 0)
		$("#btnGuardarGrupo").prop("disabled", false);

});

$(document).on("click","#btnGuardarGrupo", function()
{
	let Datos = {};

	Datos['empresa'] = $('#empresaGrupo option:selected').val();
	Datos['fechNomi'] = $('#fechaNominaGrupo option:selected').val();
	Datos['idGrupo'] = idGrupo;
	Datos['cve_trabajador'] = [];

  	$("#tblGrupoEstablecido > tbody > tr").each(function(){

		let checkNum = $(this).find('td:eq(0) input[type="checkbox"]');

		if(checkNum.is(':checked'))
		{
			Datos['cve_trabajador'].push( $(this).find("td:eq(2)").text().trim() );
		}

  	});
	
	if(Datos['cve_trabajador'].length > 0)
		agregarTrabajadorGrupo(Datos);
	

});

function agregarTrabajadorGrupo(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    	$.ajax(
	    	{
	    		data:{ datos:Datos },
	        	url: 'crudHorario.php?accion=agregarTrabajadorGrupo',
	        	type: 'post',
	        	success: function (data) 
	        	{
	        			clearModuloGrupos()
	            	if(data.trim() == '0')
	            	{
		                swal(
		                    '¡No fue posible agregar el registro!',
		                    'Volver a intentar nuevamente',
		                    'warning'
		                )
	            	}else
	            	if(data.trim() == '1')
	            	{
			            swal(
		                    '¡Alta de registro!',
		                    'Se agrego correctamente',
		                    'success'
		               	)

		            		// clearModuloGrupos();
		                cargarTblPersonal(Datos);
		                trabajadoresGrupoEstablecido(idGrupo)
		                // usuariosGrupoEstablecido(idGrupo)
		                // cargarTblGrupos(Datos);
		            }
		            else
		            {
			            	swal(
		                    '¡No es posible realizar alta de registro!',
		                    data,
		                    'warning'
		                )
		            }
		            $("#btnGuardarGrupo").prop("disabled", true);
		            $(".loader").fadeOut("fast");
	        	}
	    	});
		});
}

$(document).on("change", "#empresaHorario", function()
{
	let empresa = $(this).val();
	if(!isEmpty(empresa))
		cargarPeriodoNominaHorario(empresa);

});

function cargarPeriodoNominaHorario(empresa)
{
	// let Datos = {};
	// Datos['empresa'] = empresa;

	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominal',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
    				$("#fechaNominaHorario").find('option').remove().end().append(option);
    				let fechNomi = $("#fechaNominaHorario option:selected").val();
      				$('#fechaNominaHorario').val(fechNomi).selectpicker('refresh');
      				// cargarTblHorarios(Datos);
        		}
        		else
        		{
    				$("#fechaNominaHorario").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaHorario').val(null).selectpicker('refresh');
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

$(document).on("change", "#empresaHorarioEstablecido", function()
{
	let empresa = $(this).val();
	if(!isEmpty(empresa))
		cargarPeriodoNominaHorarioEstablecido(empresa);

});

function cargarPeriodoNominaHorarioEstablecido(empresa)
{
	let Datos = {};
	Datos['empresa'] = empresa;

	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominalHorarioEstablecido',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
    				$("#fechaNominaHorarioEstablecido").find('option').remove().end().append(option);
    				Datos['fechNomi'] = $("#fechaNominaHorarioEstablecido option:selected").val();
      				$('#fechaNominaHorarioEstablecido').val(Datos['fechNomi']).selectpicker('refresh');
      				cargarTblHorarios(Datos);
        		}
        		else
        		{
    				$("#fechaNominaHorarioEstablecido").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaHorarioEstablecido').val(null).selectpicker('refresh');
    				cleanTblHorarios()
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

$(document).on("change", "#fechaNominaHorarioEstablecido", function()
{
		let Datos = {};
		Datos['empresa'] = $("#empresaHorarioEstablecido option:selected").val();
		Datos['fechNomi'] = $(this).val(); 
		cargarTblHorarios(Datos);

});

$(document).on("click", "#btnCrearHorario", function(){

	let Datos = {};

	Datos["empresa"] = $("#empresaHorario option:selected").val();
	Datos["fechNomi"] = $("#fechaNominaHorario option:selected").val();
	Datos["nombreHorario"] = $("#nombreHorario").val();

	Datos["checkBox"] = [];
	Datos["dia"] = [];
	Datos["horaInicial"] = [];
	Datos["horaFinal"] = [];

	$("#tblAltaHorario > tbody > tr").each( function(column, td){

		// let checkNum = $(this).find('td:eq(0) input[type="checkbox"]');
		// if(checkNum.is(':checked')){
				
		Datos["checkBox"].push( $(this).find('td:eq(0) input[type="checkbox"]').is(':checked') );
		Datos["dia"].push( $(this).find('td:eq(1)').text() );
		Datos["horaInicial"].push( $(this).find("td:eq(2) .horaInicial").val() );
		Datos["horaFinal"].push( $(this).find("td:eq(3) .horaFinal").val() );
		// }
	});


	if(!isEmpty(Datos["nombreHorario"]) && Datos["dia"].length > 0)
	{
		guardarHorario(Datos);
	}
	else
	{
		swal(
	        'No fue posible agregar los datos',
	        '¡Por lo menos un dia debe estar selecionado!',
	        'warning'
		)
	}


});

function guardarHorario(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ datos:Datos },
	        url: 'crudHorario.php?accion=guardarHorario',
	        type: 'post',
	        success: function (data) 
	        {
	            if(data.trim() == '0')
	            {
	                swal(
	                    '¡No fue posible agregar el registro!',
	                    'Volver a intentar nuevamente',
	                    'warning'
	                )
	            }else
	            if(data.trim() == '1')
	            {
	            	clearFields();
	            	cargarNominaHorarioCreado(Datos);
	            	updateModAsignacionHorario(Datos);
	                // cargarTblGrupoHorario(Datos);

	                swal(
	                    '¡Alta de registro!',
	                    'Se agrego correctamente',
	                    'success'
	                )
	            }
	            else
	            {
	            	swal(
	                    '¡No se puede realizar alta de registro!',
	                    data,
	                    'warning'
	                )
	            }
	            $(".loader").fadeOut("fast");

	        }
	    });
	});
}

function clearFields()
{
	$("#nombreHorario").val(null);
	// $("#diasLaborables").val(null).selectpicker('refresh');
	$("#tblAltaHorario > tbody > tr").each( function(column, td){

		$(this).find('td:eq(0) input[type="checkbox"]').prop('checked', false);
		$(this).find("td:eq(2) .horaInicial").val(null);
		$(this).find("td:eq(3) .horaFinal").val(null);

	});

	$("#empresaHorario").prop("disabled", false);
	$("#fechaNominaHorario").prop("disabled", false);
	$("#btnCrearHorario").prop("disabled", true);
}

function cargarNominaHorarioCreado(Datos)
{

	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:Datos['empresa']},
	      	url: 'crudHorario.php?accion=periodoNominalHorarioEstablecido',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
    				$("#fechaNominaHorarioEstablecido").find('option').remove().end().append(option);
    				$('#empresaHorarioEstablecido').val(Datos['empresa']).selectpicker('refresh');
    				$('#fechaNominaHorarioEstablecido').val(Datos['fechNomi']).selectpicker('refresh');
      				cargarTblHorarios(Datos);
        		}
        		else
        		{
    				$("#fechaNominaHorarioEstablecido").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaHorarioEstablecido').val(null).selectpicker('refresh');
    				cleanTblHorarios();
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

function cargarTblHorarios(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ datos:Datos },
	        url: 'crudHorario.php?accion=tblHorarios',
	        type: 'post',
	        success: function (tr) 
	        {

	        	$("#tblHorarios").DataTable().clear().destroy();
	          	$("#tblHorarios tbody").html(tr);
				$("#tblHorarios").DataTable({
              		searching : false,
					paging : false,
					order: [[ 0, "asc" ]],
		            scrollY: '60vh',
		      		scrollCollapse: true,
              		retrieve: true
          		});

	          $(".loader").fadeOut("fast");

	        }
	    });
	});
}

function cleanTblHorarios()
{
	$("#tblHorarios").DataTable().clear().destroy();
  	$("#tblHorarios tbody").html(null);
	$("#tblHorarios").DataTable({
  		searching : false,
		paging : false,
		order: [[ 0, "desc" ]],
  		retrieve: true
	});
}


$(document).on("click", "button.btnUserHorario", function()
{
		let fila = $(this).closest("tr");
		let horarioEstablecido = fila.find('td:eq(1)').text();
		$("#addUser").text("Agregar usuario al grupo: "+horarioEstablecido);
		idHorario = $(this).attr("idHorario");
		cargarTblUsuariosHorario(idHorario);
});

function cargarTblUsuariosHorario(idHorario)
{
		$(".loader").fadeIn("slow", function()
  	{
    		$.ajax(
    		{
    				data: { idHorario:idHorario },
      			url: 'crudHorario.php?accion=cargarTblUsuariosHorario',
      			type: 'post',
      			success: function (tr) 
      			{
      				$("#tblHorarioUsuarios").DataTable().clear().destroy();
	          		$("#tblHorarioUsuarios tbody").html(tr);
					$("#tblHorarioUsuarios").DataTable({
		              	searching : false,
						paging : false,
						order: [[ 1, "asc" ]]
		          	});

		          	$("#modal_horario_usuarios").modal("show");

	          		$(".loader").fadeOut("fast");
      			}
      	});
    });
}

$(document).on("click", "input.checkUserHorario", function(){

		let countCheck = $("input.checkUserHorario:checked").length;

		$("#btnGuardarUsuarioHorario").prop("disabled", true);
		if(countCheck > 0)
				$("#btnGuardarUsuarioHorario").prop("disabled", false);

});

$(document).on("click", "#btnGuardarUsuarioHorario", function()
{
	let Datos = {};
	let countCheck = $("input.checkUserHorario:checked").length;

	Datos['idHorario'] = idHorario;
	Datos['usuarios'] = [];

  	$("#tblHorarioUsuarios > tbody > tr").each(function(){

		let checkNum = $(this).find('td:eq(0) input[type="checkbox"]');

		if(checkNum.is(':checked'))
		{
				Datos['usuarios'].push( $(this).find("td:eq(1)").text() );
		}

  	});
		
	if(countCheck > 0 && idHorario > 0 && Datos['usuarios'].length > 0)
		guardarAccesoUserHorario(Datos);
});

function guardarAccesoUserHorario(Datos)
{

		$(".loader").fadeIn("slow", function()
    {
	  		$.ajax(
	    	{
	    			data:{ datos:Datos },
	      		url: 'crudHorario.php?accion=guardarAccesoUserHorario',
	        	type: 'post',
	        	success: function (data) 
	        	{
	          		if(data.trim() == '0')
	            	{
		                swal(
		                    '¡No fue posible agregar el registro!',
		                    'Volver a intentar nuevamente',
		                    'warning'
		                )
	            	}
	            	else if(data.trim() == '1')
	            	{
	            			$("#btnGuardarUsuarioHorario").prop("disabled", true);

			            swal(
		                    '¡Alta de registro!',
		                    'Se agrego correctamente',
		                    'success'
		                )

		            }
		            else
		            {
		            	swal(
		                    '¡No se puede realizar alta de registro!',
		                    data,
		                    'warning'
		                )
		            }
	            	$(".loader").fadeOut("fast");

	        	}
	    	});
		});
}

var empresaEdit, fechaNominaEdit = '';

$(document).on("click", "button.btnEditar", function(){

	let fila = $(this).closest("tr");
	let nombreHorario = fila.find("td:eq(0)").text().trim();

	empresaEdit = $("#empresaHorarioEstablecido option:selected").val();
	fechaNominaEdit = $("#fechaNominaHorarioEstablecido option:selected").val();
	

	cargarNominaHorarioEdit(empresaEdit, fechaNominaEdit);
	$("#nombreHorario").val(nombreHorario);

	idHorario = $(this).attr("idHorario");

	consultarHorario(idHorario);

	if($("#btnCrearHorario").attr("id") === "btnCrearHorario")
	{
    	$("#btnCrearHorario").attr("class","btn btn-sm btn-success");
    	$("#btnCrearHorario").text("Actualizar");
    	$("#btnCrearHorario").attr("id","btnActualizar");
	}

	$("#btnActualizar").prop("disabled", true);
	// $("#horaInicial").val(fila.find("td:eq(1)").text().trim());
	// $("#horaFinal").val(fila.find("td:eq(2)").text().trim());
	// let arrayDias = fila.find("td:eq(3)").text().trim().split(',');
	// $("#diasLaborables").selectpicker('val', arrayDias);	

});

function cargarNominaHorarioEdit(empresa, fechaNomi)
{

	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominal',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
        			$("#empresaHorario").val(empresa).selectpicker("refresh");
					$("#empresaHorario").prop("disabled", true);
    				$("#fechaNominaHorario").find('option').remove().end().append(option);
					$("#fechaNominaHorario").val(fechaNomi).selectpicker("refresh");
					$("#fechaNominaHorario").prop("disabled", true);
      				
        		}
        		else
        		{
    				$("#fechaNominaHorario").html("");
    				$('#fechaNominaHorario').val(null).selectpicker('refresh');
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

function consultarHorario(idHorario)
{
	$(".loader").fadeIn("slow", function()
  	{
    	$.ajax(
    	{
    		data:{ idHorario:idHorario },
      		url: 'crudHorario.php?accion=consultarHorario',
      		type: 'post',
      		dataType : "json",
		  	success: function (data) 
		  	{

		    	if(data.length > 0)
		    	{
		    		$("#tblAltaHorario > tbody > tr").each( function(i, td){

		    		let boolean = (data[i].checkBox == 'true') ? true : false;

		    		if(data[i].dia == 'Lunes')
		    		{
		    			$('input.checkLun').prop("checked", boolean);
		    			$(this).find('td:eq(2) .horaInicial').val(data[i].horaInicial);
		    				$(this).find('td:eq(3) .horaFinal').val(data[i].horaFinal);
		    		}
		    		if(data[i].dia == 'Martes')
		    		{
		    			$('input.checkMar').prop("checked", boolean);
		    			$(this).find('td:eq(2) .horaInicial').val(data[i].horaInicial);
		    				$(this).find('td:eq(3) .horaFinal').val(data[i].horaFinal);
		    		}
		    		if(data[i].dia == 'Miercoles')
		    		{
		    			$('input.checkMie').prop("checked", boolean);
		    			$(this).find('td:eq(2) .horaInicial').val(data[i].horaInicial);
		    				$(this).find('td:eq(3) .horaFinal').val(data[i].horaFinal);
		    		}
		    		if(data[i].dia == 'Jueves')
		    		{
		    			$('input.checkJue').prop("checked", boolean);
		    			$(this).find('td:eq(2) .horaInicial').val(data[i].horaInicial);
		    				$(this).find('td:eq(3) .horaFinal').val(data[i].horaFinal);
		    		}
		    		if(data[i].dia == 'Viernes')
		    		{
		    			$('input.checkVie').prop("checked", boolean);
		    			$(this).find('td:eq(2) .horaInicial').val(data[i].horaInicial);
		    				$(this).find('td:eq(3) .horaFinal').val(data[i].horaFinal);
		    		}
		    		if(data[i].dia == 'Sábado')
		    		{
		    			$('input.checkSab').prop("checked", boolean);
		    			$(this).find('td:eq(2) .horaInicial').val(data[i].horaInicial);
		    				$(this).find('td:eq(3) .horaFinal').val(data[i].horaFinal);
		    		}
		    		if(data[i].dia == 'Domingo')
		    		{
		    			$('input.checkDom').prop("checked", boolean);
		    			$(this).find('td:eq(2) .horaInicial').val(data[i].horaInicial);
		    				$(this).find('td:eq(3) .horaFinal').val(data[i].horaFinal);
		    		}

		    	});
		    }

		    $(".loader").fadeOut("fast");

    	}
    });
	});
}

$(document).on("click", "button.btnEliminarGrupo", function(){

	let Datos = {};
	Datos["empresa"] = $("#empresaGrupoEstablecido option:selected").val();
	Datos["fechNomi"] = $("#fechaNominaGrupoEstablecido option:selected").val();
	Datos["idGrupo"] = $(this).attr("idGrupo");

  	swal({
    	title: '¿Está seguro de borrar el grupo?',
    	text: "¡Si no lo está puede cancelar la accíón!",
    	type: 'warning',
    	showCancelButton: true,
    	confirmButtonColor: '#3085d6',
      	cancelButtonColor: '#d33',
      	cancelButtonText: 'Cancelar',
      	confirmButtonText: 'Si, borrar grupo!'
  	}).then(function(result){

	    if(result.value){

	      // window.location = "index.php?ruta=usuarios&idHorario="+idHorario;
	    	deleteGrupo(Datos);

	    }

	});

});

function deleteGrupo(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ idGrupo:Datos["idGrupo"] },
	        url: 'crudHorario.php?accion=deleteGrupo',
	        type: 'post',
	        success: function (data) 
	        {

	            if(data.trim() === "0")
                {
                    swal(
	                    'Eliminación de registro',
	                    '¡No fue posible eliminar el registro, intentar nuevamente!',
	                    'warning'
	                )
                }
                else if(data.trim() === "1")
                {
                	$("#addAddress").val("");
                	swal(
	                    'Eliminación de registro',
	                    '¡El registro se elimino correctamente!',
	                    'success'
	                )

	                cargarTblGrupos(Datos);
	                cargarTblPersonal(Datos);

                }
                else
                {
                    swal(
	                    'No es posible eliminar el registro',
	                    data.trim(),
	                    'warning'
	                )
                }

                $(".loader").fadeOut("fast");

	        }
	    });
	});
}

$(document).on("click", ".btnEliminar", function(){

	let Datos = {};
	Datos["empresa"] = $("#empresaHorarioEstablecido option:selected").val();
	Datos["fechNomi"] = $("#fechaNominaHorarioEstablecido option:selected").val();
	Datos["idHorario"] = $(this).attr("idHorario");

  	swal({
    	title: '¿Está seguro de borrar el horario?',
    	text: "¡Si no lo está puede cancelar la accíón!",
    	type: 'warning',
    	showCancelButton: true,
    	confirmButtonColor: '#3085d6',
      	cancelButtonColor: '#d33',
      	cancelButtonText: 'Cancelar',
      	confirmButtonText: 'Si, borrar horario!'
  	}).then(function(result){

	    if(result.value){

	      // window.location = "index.php?ruta=usuarios&idHorario="+idHorario;
	    	deleteHorario(Datos);

	    }

	});

});

function deleteHorario(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ idHorario:Datos["idHorario"] },
	        url: 'crudHorario.php?accion=deleteHorario',
	        type: 'post',
	        success: function (data) 
	        {

	            if(data.trim() === "0")
                {
                    swal(
	                    'Eliminación de registro',
	                    '¡No fue posible eliminar el registro, intentar nuevamente!',
	                    'warning'
	                )
                }
                else if(data.trim() === "1")
                {
                	$("#addAddress").val("");
                	swal(
	                    'Eliminación de registro',
	                    '¡El registro se elimino correctamente!',
	                    'success'
	                )

	                cargarTblHorarios(Datos)

                }
                else
                {
                    swal(
	                    'No es posible eliminar el registro',
	                    data.trim(),
	                    'warning'
	                )
                }

                $(".loader").fadeOut("fast");

	        }
	    });
	});
}

$(document).on("click", "input.checkDia", function(){

	let fechNomi = $("#fechaNominaHorario option:selected").val();
	let nombreHorario = $('#nombreHorario').val();
	let countCheck = $("input.checkDia:checked").length;

	$("#btnCrearHorario").prop("disabled", true);
	$("#btnActualizar").prop("disabled", true);
	if(!isEmpty(nombreHorario) && countCheck > 0 && !isEmpty(fechNomi))
	{
			$("#btnCrearHorario").prop("disabled", false);
			$("#btnActualizar").prop("disabled", false);
	}

});

$(document).on("keyup change", "input#nombreHorario", function()
{
	let fechNomi = $("#fechaNominaHorario option:selected").val();
	let nombreHorario = $(this).val();
	let countCheck = $("input.checkDia:checked").length;

	$("#btnCrearHorario").prop("disabled", true);
	$("#btnActualizar").prop("disabled", true);
	if(!isEmpty(nombreHorario) && countCheck > 0 && !isEmpty(fechNomi))
	{
		$("#btnCrearHorario").prop("disabled", false);
		$("#btnActualizar").prop("disabled", false);
	}
});

$(document).on("keyup", "input.horaInicial, input.horaInicial", function()
{
	let fechNomi = $("#fechaNominaHorario option:selected").val();
	let nombreHorario = $(this).val();
	let countCheck = $("input.checkDia:checked").length;

	$("#btnCrearHorario").prop("disabled", true);
	$("#btnActualizar").prop("disabled", true);
	if(!isEmpty(nombreHorario) && countCheck > 0 && !isEmpty(fechNomi))
	{
		$("#btnCrearHorario").prop("disabled", false);
		$("#btnActualizar").prop("disabled", false);
	}
});

$(document).on("click","#btnCancelar",function(){

    $("#nombreHorario").val(null);
    $("input.horaInicial").val(null);
    $("input.horaFinal").val(null);
    $("input.checkLun").prop("checked", false);
    $("input.checkMar").prop("checked", false);
    $("input.checkMie").prop("checked", false);
    $("input.checkJue").prop("checked", false);
    $("input.checkVie").prop("checked", false);
    $("input.checkSab").prop("checked", false);
    $("input.checkDom").prop("checked", false);

	if($("#btnActualizar").attr("id") === "btnActualizar")
	{
    	$("#btnActualizar").attr("class","btn btn-sm btn-primary");
        $("#btnActualizar").text("Crear");
        $("#btnActualizar").attr("id","btnCrearHorario");
	}

    // $("#idAceptar").prop("disabled",true);

});

$(document).on("click", "#btnActualizar", function(){

	let Datos = {};
	Datos["empresa"] = $("#empresaHorario option:selected").val();
	Datos["fechNomi"] = $("#fechaNominaHorario option:selected").val(); 
	Datos["idHorario"] = idHorario;
	Datos["nombreHorario"] = $("#nombreHorario").val();
	Datos["checkBox"] = [];
	Datos["dia"] = [];
	Datos["horaInicial"] = [];
	Datos["horaFinal"] = [];

	$("#tblAltaHorario > tbody > tr").each( function(column, td){

		Datos["checkBox"].push( $(this).find('td:eq(0) input[type="checkbox"]').is(':checked') );
		Datos["dia"].push( $(this).find('td:eq(1)').text() );
		Datos["horaInicial"].push( $(this).find("td:eq(2) .horaInicial").val() );
		Datos["horaFinal"].push( $(this).find("td:eq(3) .horaFinal").val() );

	});

	// Datos["horaInicial"] = $("#horaInicial").val();
	// Datos["horaFinal"] = $("#horaFinal").val();
	// Datos["diasLaborables"] = $("#diasLaborables").val();

	if(!isEmpty(Datos["nombreHorario"]) && validateCheckBox(Datos["checkBox"])){
		
		if(Datos["empresa"] === empresaEdit && Datos["fechNomi"] === fechaNominaEdit)
		{
			editarHorario(Datos)
		
		}
		else
		{
			let timerInterval

			swal({

                type: "warning",
                title: "Los datos fueron alterados",
                text: "¡Se volvera a recargar la página!",
                showConfirmButton: false,
				timer: 3000,
				onBeforeOpen: () => {
					swal.showLoading()
					timerInterval = setInterval(() => {
					const content = Swal.getContent()
						if (content) {
						    const b = content.querySelector("b")
						    if (b) {
						        b.textContent = Swal.getTimerLeft()
						    }
						}
					}, 100)
				},
				onClose: () => {
					clearInterval(timerInterval)
				}
            });
            
            setTimeout(loadPage, 3000);

            
		}
	}
	else
	{
		swal(
	        'No fue posible agregar los datos',
	        '¡Por lo menos un dia debe estar selecionado!',
	        'warning'
	    )
	}

});

function loadPage()
{
	location.reload();
}

function editarHorario(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ datos:Datos },
	        url: 'crudHorario.php?accion=editarHorario',
	        type: 'post',
	        success: function (data) 
	        {
	            if(data.trim() == '0')
	            {
	                swal(
		                '¡Edición de registro!',
		                '¡No se realizó ninguna modificación!',
		                'warning'
		            )
		            
		            clearFields();

		            if($("#btnActualizar").attr("id") === "btnActualizar")
					{
				    	$("#btnActualizar").attr("class","btn btn-sm btn-primary");
				        $("#btnActualizar").text("Crear");
				        $("#btnActualizar").attr("id","btnCrearHorario");
				        $("#btnCrearHorario").prop("disabled", true);
					}
	            }
	            else if(data.trim() == '1')
	            {
	            	swal(
		                '¡Edición de registro!',
		                'El registro se edito correctamente',
		                'success'
		            )
            		clearFields();
                	cargarTblHorarios(Datos);
	            }
	            else
	            {
	            	swal(
	                    '¡No fue posible editar el registro!',
	                    data,
	                    'warning'
	                )
	            }
	            
	            $(".loader").fadeOut("fast");

	        }
	    });
	});
}

function validateCheckBox(checkBox)
{
	let boolean = false;
	jQuery.each( checkBox, function( i, val ) {
		if(val)
			boolean = true;
	});
	return boolean;
}

$(document).on("change", "#empresa", function(){
		
	empresaGrupoHorario = $(this).val();
	if(!isEmpty(empresaGrupoHorario))
	{
		cargarNominaGrupoHorario(empresaGrupoHorario);
	}
});

function cargarNominaGrupoHorario(empresa)
{
	let Datos = {};
	Datos['empresa'] = empresa;
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominalGrupoEstablecido',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
    				$("#fechaNomina").find('option').remove().end().append(option);
    				Datos['fechNomi'] = $("#fechaNomina option:selected").val();
      				$('#fechaNomina').val(Datos['fechNomi']).selectpicker('refresh');
      				cargarTblGrupoHorario(Datos);
        		}
        		else
        		{
    				$("#fechaNomina").find('option').remove().end().append('<option></option>');
    				$('#fechaNomina').val(null).selectpicker('refresh');
    				cleanTblGrupoHorario()
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

$(document).on("change", "#fechaNomina", function()
{
		let Datos = {};
		Datos['empresa'] = $("#empresa option:selected").val();
		Datos['fechNomi'] = $(this).val(); 
		cargarTblGrupoHorario(Datos);

});


function cargarTblGrupoHorario(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
  		$.ajax(
    	{
    		data:{ datos:Datos },
        	url: 'crudHorario.php?accion=cargarTblGrupoHorario',
        	type: 'post',
        	success: function (tr) 
        	{

        		$("#tblGrupoHorario").DataTable().clear().destroy();
            	$("#tblGrupoHorario tbody").html(tr);
           		$("#tblGrupoHorario").DataTable({
       				searching : false,
					paging : false,
					// "columnDefs": [
				 //  		{ "width": "13%", "targets": 0 },
				 //  		{ "width": "29%", "targets": 1 },
				 //  		{ "width": "29%", "targets": 2 },
				 //  		{ "width": "29%", "targets": 3 }
					// ],
		            retrieve: true
          		});

            	$("select.horario").selectpicker('refresh');
            	$("select.ubicacion").selectpicker('refresh');

            	let rowCount = $('#tblGrupoHorario tbody tr').length;

            	$("#btnGuardar").prop('disabled', true);
            	if(rowCount > 0)
            			$("#btnGuardar").prop('disabled', false);

            	$(".loader").fadeOut("fast");

        	}
    	});
	});
}

function cleanTblGrupoHorario()
{
	$("#tblGrupoHorario").DataTable().clear().destroy();
	$("#tblGrupoHorario tbody").html(null);
	$("#tblGrupoHorario").DataTable({
		searching : false,
		paging : false,
    	retrieve: true
	});
}

function updateModAsignacionHorario(Datos)
{

	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:Datos['empresa']},
	      	url: 'crudHorario.php?accion=periodoNominalGrupoEstablecido',
	      	type: 'post',
	      	success: function (option) 
	      	{
        		if(option.trim() != 'No existen datos')
        		{
        			$("#empresa").val(Datos['empresa']).selectpicker("refresh");
    				$("#fechaNomina").find('option').remove().end().append(option);
      				$('#fechaNomina').val(Datos['fechNomi']).selectpicker('refresh');
      				cargarTblGrupoHorario(Datos);
        		}
        		else
        		{
    				$("#fechaNomina").html("");
    				$('#fechaNomina').val(null).selectpicker('refresh');
        		}
        		
        		$(".loader").fadeOut("fast");
	      	}
	    });
	});
}

$(document).on("change", "select.horario", function()
{
		let val = $(this).val();
		
		if(isEmpty(val))
				$(this).val(null).selectpicker('refresh');
		
});

$(document).on("change", "select.ubicacion", function()
{
		let val = $(this).val();
		
		if(isEmpty(val))
				$(this).val(null).selectpicker('refresh');
		
});


// function ctgDepartamentos(empresa)
// {
// 	$(".loader").fadeIn("slow", function()
//     {
// 	    $.ajax(
// 	    {
// 	    	data:{ empresa:empresa },
// 	        url: 'crudHorario.php?accion=ctgDepartamentos',
// 	        type: 'post',
// 	        success: function (option) 
// 	        {
// 	            $("#departamento").find('option').remove().end().append(option);	            
// 	            $("#departamento").selectpicker('refresh');
// 	            $(".loader").fadeOut("fast");

// 	        }
// 	    });
// 	});
// }

// $(document).on("change", "#departamento", function(){

// 	let Datos = {};

// 	Datos["empresa"] = $("#empresa option:selected").val();
// 	Datos["departamento"] = $(this).val();

// 	ctgEmpleados(Datos);

// });

// function ctgEmpleados(Datos)
// {
// 	$(".loader").fadeIn("slow", function()
//     {
// 	    $.ajax(
// 	    {
// 	    	data:{ datos:Datos },
// 	        url: 'crudHorario.php?accion=ctgEmpleados',
// 	        type: 'post',
// 	        success: function (tr) 
// 	        {
// 	        		// $("#tblEmpleados").DataTable().clear().destroy();
// 	            $("#tblEmpleados tbody").html(tr);
// 	           //  $("#tblEmpleados").DataTable({
// 	           //    scrollY: (screen.height-600)+'px',
// 	           //    scrollX: true,
// 	           //    retrieve: true
// 	          	// });

// 	            // $(".selectpicker").selectpicker('refresh');
// 	            $(".loader").fadeOut("fast");

// 	            let rowCount = $('#tblEmpleados tbody tr').length;

// 	            $("#btnGuardar").prop('disabled', true);
// 	            if(rowCount > 0)
// 	            	$("#btnGuardar").prop('disabled', false);

// 	        }
// 	    });
// 	});
// }


$(document).on("change", "#ubicacion", function(){

		let ctlUbicacion = $(this).val();
    if(ctlUbicacion == "Definir nuevo")
    {
    	$(this).val(null).trigger('change');
    	ctgUbicacion();
    	
    }

});

function ctgUbicacion()
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	        url: 'crudHorario.php?accion=ctgUbicacion',
	        type: 'post',
	        success: function (tr) 
	        {
	            $("#tblCtlUbicacion tbody").html(tr);
	            $("#modal_ctl_ubicacion").modal("show");
	            $(".loader").fadeOut("fast");

	        }
	    });
	});
}

$(document).on("keyup change","#addUbicacion, #addCtaContable",function()
{
		$("#btnAgregar").prop('disabled', true);
		if($(this).val().length > 0)
				$("#btnAgregar").prop('disabled', false);
});

$(document).on("click","#btnAgregar",function()
{
    let Datos = {};
    Datos['addUbicacion'] = $("#addUbicacion").val();
    Datos['addCtaContable'] = $("#addCtaContable").val();

    if(!isEmpty(Datos['addUbicacion']) && !isEmpty(Datos['addCtaContable']))
    {
	    $(".loader").fadeIn("slow", function(){
	        $.ajax(
	        {
	            data:{datos:Datos},
	            url: 'crudHorario.php?accion=agregarUbicacion',
	            type: 'post',
	            success: function (data) 
	            {
	                $(".loader").fadeOut("fast");

	                if(data.trim() === "0")
	                {
	                    swal(
		                    'No fue posible realizar el alta',
		                    '¡Intentar nuevamente!',
		                    'warning'
		                )
	                }
	                else if(data.trim() === "1")
	                {
	                	$("#addAddress").val("");
	                	swal(
		                    'Alta de Ubicación y Cta Contable',
		                    'Se agrego correctamente!',
		                    'success'
		                )
	                    ctgUbicacion();
	                    refreschSelectUbicacion();
	                }
	                else if(data.trim() === "Registro ya existente")
	                {
	                    swal(
		                    'Alta de ubicación y Cta Contable!',
		                    data.trim(),
		                    'warning'
		                )
	                }
	             
	            }

	        });
	    });
	}

});

function refreschSelectUbicacion()
{
	$(".loader").fadeIn("slow", function(){

		$.ajax(
	    {
	        url: 'crudHorario.php?accion=refreschSelectUbicacion',
	        type: 'post',
	        success: function (option) 
	        {
	            $(".loader").fadeOut("fast");
	            
	            $("#ubicacion").find('option').remove().end().append(option+'<option data-divider="true"></option><option>Definir nuevo</option>');
	            // $("#editAddress").append(option);
	            
	            $('#ubicacion').selectpicker('refresh');
	            // $('#editAddress').selectpicker('refresh');
	        }
	    });

	});
}

$(document).on("click", "#btnBuscar",function(){

	let Datos = {};
	Datos['empresaReporte'] = $("#empresaReporte option:selected").val();
	Datos['departamentoReporte'] = $("#departamentoReporte option:selected").text();
	Datos['fechaNominaAsistencias'] = $("#fechaNominaAsistencias").val();
	// Datos["fInicial"] = $("#fInicial").val();
	// Datos["fFinal"] = $("#fFinal").val();
	if(!isEmpty(Datos["empresaReporte"]) && !isEmpty(Datos["departamentoReporte"]) && !isEmpty(Datos["fechaNominaAsistencias"]))
		datosAsistencias(Datos)

});

function datosAsistencias(Datos)
{
	// $(".loader").fadeIn("slow", function()
 //    {
 	$("#loadTblAsistencias").LoadingOverlay("show", {
	    image       : "../img/logos/logo-icon.png",
	    imageAnimation  : "1.5s fadein"
	});

    $.ajax(
    {
    	data:{ datos:Datos },
        url: 'crudHorario.php?accion=asistencias',
        type: 'post',
        success: function (tr) 
        {

        	let count     = 0;
      		let interval  = setInterval(function(){
	    		if (count >= 40) {
					clearInterval(interval);
		        	$("#tblAsistencias").DataTable().clear().destroy();
		        	$("#tblAsistencias tbody").html(tr);
		            $("#tblAsistencias").DataTable({
			            scrollY: '50vh',
			            scrollCollapse: true,
			            retrieve: true
		          	});

					$("#loadTblAsistencias").LoadingOverlay("hide", true);

		        }
		        count += 10;
	        }, 300);
        }
    });
	// });
}

$(document).on("click", "#btnReporte", function(){
	
	reporteAsistencias()

});

function reporteAsistencias()
{
	window.open("crudHorario.php?accion=reporteAsistencias", 'TheWindow',"toolbar=yes,scrollbars=yes,resizable=yes,top=50,left=100,width=1700,height=900");
}

// $(document).on("click", "input.checkTrabajador", function(){

// 	contChecked = $("input.checkTrabajador:checked").length;
// 	// console.log("contChecked", contChecked);

// });

$(document).on("click", "#btnBuscarListaAsistencia",function(){

	let Datos = {};
	Datos['empresaReporte'] = $("#empresaListaAsistencia option:selected").val();
	Datos['departamentoReporte'] = $("#grupoListaAsistencia option:selected").text(); ///grupo
	Datos['fechaNominaAsistencias'] = $("#fechaNominaListaAsistencia option:selected").val();
	// Datos["fInicial"] = $("#fInicial").val();
	// Datos["fFinal"] = $("#fFinal").val();
	if(!isEmpty(Datos["empresaReporte"]) && !isEmpty(Datos["departamentoReporte"]) && !isEmpty(Datos["fechaNominaAsistencias"]))
		datosListaAsistencias(Datos)

});

function datosListaAsistencias(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ datos:Datos },
	        url: 'crudHorario.php?accion=listaAsistencias',
	        type: 'post',
	        success: function (tr) 
	        {
	        	$("#tblListaAsistencia").DataTable().clear().destroy();
            	$("#tblListaAsistencia tbody").html(tr);
	            $("#tblListaAsistencia").DataTable({
		            scrollY: '50vh',
		            scrollCollapse: true,
		            order: [[ 2, "asc" ], [ 4, 'asc' ], [ 5, 'asc' ]],
		            retrieve: true
	          	});
	          
	          // $(".selectpicker").selectpicker('refresh');

	        	$(".loader").fadeOut("fast");

	        }
	    });
	});
}

$(document).on("click", "#btnReporteListaAsistencia", function(){
	
	reporteListaAsistencias()

});

function reporteListaAsistencias()
{
	window.open("crudHorario.php?accion=reporteListaAsistencias", 'TheWindow',"toolbar=yes,scrollbars=yes,resizable=yes,top=50,left=100,width=1700,height=900");
}

$(document).on("click", "#btnGuardar", function(){
		
		let Datos = {};

		Datos["empresa"] = $("#empresa option:selected").val();
		Datos["fechNomi"] = $("#fechaNomina option:selected").val();
		Datos["grupo"] = [];
		Datos["horario"] = [];
		Datos["ubicacion"] = [];

		$("#tblGrupoHorario > tbody > tr").each(function(){

	      	let grupo = $(this).find("td:eq(1)").attr("idGrupo");
	      	let horario = $(this).find("td:eq(2) .horario option:selected").val();
	      	let ubicacion = $(this).find("td:eq(3) .ubicacion option:selected").val();

	      	if(grupo != undefined && horario != undefined && ubicacion != undefined)
	      	{
	  			Datos["grupo"].push( grupo );
	  			Datos["horario"].push( horario );
	  			Datos["ubicacion"].push( ubicacion );
	      	}

		});

		if(!isEmpty(Datos["empresa"]) && Datos["grupo"].length > 0 && Datos["horario"].length > 0 && Datos["ubicacion"].length > 0)
			guardarHorarioAsignado(Datos);
		// console.log("Datos", Datos);

});

function guardarHorarioAsignado(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
  		$.ajax(
    	{
    		data: { datos:Datos },
      		url: 'crudHorario.php?accion=guardarHorarioAsignado',
        	type: 'post',
        	success: function (data) 
        	{
          		if(data.trim() == '0')
            	{
	                swal(
	                    'Asignación de Horario',
	                    '¡No se realizo ningún cambio!',
	                    'warning'
	                )
	                $(".loader").fadeOut("fast");
            	}
            	else if(data.trim() == '1')
            	{
            			swal(
	                    'Asignación de Horario',
	                    '¡Se realizó correctamente!',
	                    'success'
            		)
        			// clearFields()
          	// 		cargarTblHorarios()
          			cargarPeriodoNominal(Datos["empresa"])
          			cargarPeriodoNominaPP(Datos["empresa"])
            	}
            	else
            	{
            		swal(
                  		'¡No fue posible editar el registro!',
                    	data,
                    	'warning'
                	)
                	$(".loader").fadeOut("fast");
            	}
            
            	// $(".loader").fadeOut("fast");
        	}
    	});
	});
}

// function guardarDepartamento(Datos)
// {
// 	$(".loader").fadeIn("slow", function(){

// 		$.ajax(
// 	    {
// 	        data:{datos:Datos},
// 	        url: 'crudHorario.php?accion=guardarDepartamento',
// 	        type: 'post',
// 	        success: function (data) 
// 	        {
// 	        	$(".loader").fadeOut("fast");

// 	        	if(data.trim() === "0")
// 	            {
// 	                swal(
// 		                'Departamento por trabajador',
// 		                '¡No se realizó ningun modificación!',
// 		                'warning'
// 		            )
// 	            }
// 	            else if(data.trim() === "1")
// 	            {
// 	            	$("#addAddress").val("");
// 	            	swal(
// 		                'Departamento por trabajador',
// 		                '¡Los datos se agregaron correctamente!',
// 		                'success'
// 		            )
// 	            }
// 	            else if(data.trim() === "Registro ya existente")
// 	            {
// 	                swal(
// 		                '¡Error interno!',
// 		                data.trim(),
// 		                'warning'
// 		            )
// 	            }
// 	        }
// 	    });

// 	});
// }

$(document).on("change", "#empresaHorasExtras", function(){
	
	let empresa = $(this).val();
	if(!isEmpty(empresa))
	{
		ctgDepartamentosHorasExtras(empresa)
	}

});

// $(document).on("change", "#fechaNominaHorasExtras", function()
// {
// 	let Datos = {};

// 	Datos['empresa'] = $("#empresaHorasExtras option:selected").val();
// 	Datos['fechNomi'] = $("#fechaNominaHorasExtras option:selected").val();

// 	if(!isEmpty(Datos['empresa']) && !isEmpty(Datos['fechNomi']))
// 		cargarGrupoHorarioAsignado(Datos);

// });

function ctgDepartamentosHorasExtras(empresa)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ empresa:empresa },
	        url: 'crudHorario.php?accion=ctgDepartamentos',
	        type: 'post',
	        success: function (option) 
	        {
	            $("#departamentoHorasExtras").find('option').remove().end().append(option);	            
	            $("#departamentoHorasExtras").selectpicker('refresh');
	            cargarPeriodoNominal(empresa)
	            // $(".loader").fadeOut("fast");

	        }
	    });
	});
}

function cargarPeriodoNominal(empresa)
{
	// let Datos = {};
	// Datos['empresa'] = empresa;
	$(".loader").fadeIn("slow", function()
    {
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominalAsignacionGrupoHorario',
	      	type: 'post',
	      	success: function (option) 
	      	{
	        	if(option.trim() != 'No existen datos')
	        	{
	        		$("#empresaHorasExtras").val(empresa).selectpicker("refresh");
	        		$("#fechaNominaHorasExtras").find('option').remove().end().append(option);
	        		// Datos['fechNomi'] = $("#fechaNominaHorasExtras option:selected").val();
	          		$('#fechaNominaHorasExtras').selectpicker('refresh');
	          		// cargarGrupoHorarioAsignado(Datos)
	        	}
	        	else
		        {
		        	$("#fechaNominaHorasExtras").find('option').remove().end().append('<option></option>');
		        	$('#fechaNominaHorasExtras').selectpicker('refresh');
		        }

		        $(".loader").fadeOut("fast");

	      	}
	    });
	});
}

// function cargarGrupoHorarioAsignado(Datos)
// {
// 	$(".loader").fadeIn("slow", function()
//     {
// 		$.ajax(
// 	    {
// 	    	data:{datos:Datos},
// 	      	url: 'crudHorario.php?accion=cargarGrupoHorarioAsignado',
// 	      	type: 'post',
// 	      	success: function (option) 
// 	      	{
// 	        	if(option.trim() != 'No existen datos')
// 	        	{
// 	        		$("#grupoHorarioAsignado").find('option').remove().end().append(option);
// 	          		$('#grupoHorarioAsignado').selectpicker('refresh');

// 	          		$("#btnBuscarHorasExtras").prop("disabled", false);
// 	        	}
// 	        	else
// 		        {
// 		        	$("#grupoHorarioAsignado").find('option').remove().end().append('');
// 		        	$('#grupoHorarioAsignado').selectpicker('refresh');

// 		        	$("#btnBuscarHorasExtras").prop("disabled", true);
// 		        }
// 		        $(".loader").fadeOut("fast");

// 	     	}
// 	    });
//     });
// }

$(document).on("change", "#empresaReporte", function(){
	let empresa = $(this).val();
	if(!isEmpty(empresa))
	{
		ctgDepartamentoReporte(empresa);
	}
});

function ctgDepartamentoReporte(empresa)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ empresa:empresa },
	        url: 'crudHorario.php?accion=ctgDepartamentos',
	        type: 'post',
	        success: function (option) 
	        {

	            $("#departamentoReporte").find('option').remove().end().append(option);	            
	            $("#departamentoReporte").selectpicker('refresh');
	            cargarPeriodoNominalReporte(empresa)
	            // $(".loader").fadeOut("fast");

	        }
	    });
	});
}

function cargarPeriodoNominalReporte(empresa)
{
	$.ajax(
    {
    	data:{empresa:empresa},
      	url: 'crudHorario.php?accion=periodoNominal',
      	type: 'post',
      	success: function (option) 
      	{
	        if(option.trim() != 'No existen datos')
	        {
	        	$("#fechaNominaAsistencias").find('option').remove().end().append(option);
	          	$('#fechaNominaAsistencias').selectpicker('refresh');
	        }
	        else
	        {
	        	$("#fechaNominaAsistencias").find('option').remove().end().append('<option></option>');
	        	$('#fechaNominaAsistencias').selectpicker('refresh');
	        }

	        $(".loader").fadeOut("fast");

      	}
    });
}


$(document).on("change", "#empresaListaAsistencia", function(){
	let empresa = $(this).val();
	if(!isEmpty(empresa))
	{
		ctgGrupoListaAsistencia(empresa);
	}
});

function ctgGrupoListaAsistencia(empresa)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ empresa:empresa },
	        url: 'crudHorario.php?accion=ctgGrupos',
	        type: 'post',
	        success: function (option) 
	        {

	            $("#grupoListaAsistencia").find('option').remove().end().append(option);	            
	            $("#grupoListaAsistencia").selectpicker('refresh');
	            cargarPeriodoNominalListaAsistencia(empresa)
	            // $(".loader").fadeOut("fast");

	        }
	    });
	});
}


function cargarPeriodoNominalListaAsistencia(empresa)
{
	$.ajax(
    {
    	data:{empresa:empresa},
      	url: 'crudHorario.php?accion=periodoNominal',
      	type: 'post',
      	success: function (option) 
      	{
	        if(option.trim() != 'No existen datos')
	        {
	        	$("#fechaNominaListaAsistencia").find('option').remove().end().append(option);
	          	$('#fechaNominaListaAsistencia').selectpicker('refresh');
	        }
	        else
	        {
	        	$("#fechaNominaListaAsistencia").find('option').remove().end().append('<option></option>');
	        	$('#fechaNominaListaAsistencia').selectpicker('refresh');
	        }

	        $(".loader").fadeOut("fast");

      	}
    });
}

/**
 * @description Funcion que sirve para obtener el grupo en base al departamento en horas extras
 */
 $(document).on("change", "#fechaNominaHorasExtras", function(){ 
	grupoAsignado()
});
$(document).on("change", "#departamentoHorasExtras", function(){
	grupoAsignado();
});

function grupoAsignado() {
	let Datos = {};
	empresa = $("#empresaHorasExtras option:selected").val();
	departamento = $("#departamentoHorasExtras option:selected").val();
	departamentoText = $("#departamentoHorasExtras option:selected").text();
	fechNomi = $("#fechaNominaHorasExtras option:selected").val();
	Datos = {empresa:empresa,departamento:departamento,departamentoText:departamentoText,fechNomi:fechNomi};
	$("#grupoAsignadoUsu").empty();
	// $("#grupoAsignadoUsu").selectpicker("refresh");
	$("#grupoAsignadoUsu").selectpicker('deselectAll');
	$.ajax({
		data: Datos,
		url: 'crudHorario.php?accion=grupoAsignadoUsu',
		type: 'post',
		dataType:'json',
		success: function (data){
			if (data.length > 0) {
				data.forEach(element => {
					$('#grupoAsignadoUsu').append(`<option value="${element.id_grupo}">${element.nombre_grupo}</option>`);
					$("#grupoAsignadoUsu").selectpicker('deselectAll');
					$("#grupoAsignadoUsu").selectpicker("refresh");
				});
			}else{
				$("#grupoAsignadoUsu").empty();
				$('#grupoAsignadoUsu').append(`<option value="" disabled>No hay grupos</option>`);
				$("#grupoAsignadoUsu").selectpicker('deselectAll');
				$("#grupoAsignadoUsu").selectpicker("refresh");
			}
		}
	});
}

$(document).on("click", "#btnBuscarHorasExtras", function()
{
	let Datos = {};

	empresa = $("#empresaHorasExtras option:selected").val();
	departamento = $("#departamentoHorasExtras option:selected").val();
	departamentoText = $("#departamentoHorasExtras option:selected").text();
	fechNomi = $("#fechaNominaHorasExtras option:selected").val();
	grupo = $("#grupoAsignadoUsu option:selected");
	selected = [];
	$(grupo).each(function(){
		selected.push($(this).val()
		);
	});

	Datos["empresa"] = empresa;
	Datos["departamento"] = departamento;
	Datos["departamentoHorasExtras"] = departamentoText;
	Datos["fechNomi"] = fechNomi;
	Datos["grupoAsignado"] = selected;

	Datos["fInicialHorasExtras"] = $("#fInicialHorasExtras").val();
	Datos["fFinalHorasExtras"] = $("#fFinalHorasExtras").val();	
	if(!isEmpty(Datos["empresa"]) && !isEmpty(Datos["departamento"]) && !isEmpty(Datos["fechNomi"]) && !isEmpty(Datos["grupoAsignado"]) )
	{
		reporteHorasExtras(Datos);

		validarInfoGuardada(Datos);
	}else{
		swal({
			title: "No han sido llenados todos los filtros",
			text: "¡Se deben llenar todos los filtros para poder mostrar información!",
			type: "warning",
			confirmButtonText: "¡Cerrar!"
		});
	}

});

function reporteHorasExtras(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ datos:Datos },
	        url: 'crudHorario.php?accion=reporteHorasExtras',
	        type: 'post',
	        success: function (tr) 
	        {
	        	$("#btnGuardarHrsExtras").prop("disabled", true);

	        	if(tr.trim() == 'No hay datos')
	        	{
					$("#grupoAsignadoUsu").empty();
					$("#grupoAsignadoUsu").selectpicker('deselectAll');
					$("#grupoAsignadoUsu").selectpicker("refresh");
	        		$("#tblHorasExtras tbody").html(null);
	        		swal({
		                title: "No hay datos para calcular horas extras",
		                text: "¡Se debe asignar un horario a un grupo!",
		                type: "warning",
		                confirmButtonText: "¡Cerrar!"
		            });

		            $("#btnGuardarHrsExtras").prop("disabled", true);

	        	}
	        	else if(tr.trim() != ''){
	        		
	        		// console.log("enable", tr);
	        		$("#tblHorasExtras").DataTable().clear().destroy();
		        	$("#tblHorasExtras tbody").html(tr);
		        	$("#tblHorasExtras").DataTable({
	              		searching : true,
						paging : false,
				        scrollY: '60vh',
				      	scrollCollapse: true,
	              		scrollX: true,
	              		retrieve: true
	          		});
	          	
		        	$('.timepicker').timepicker({
				    	showInputs: false,
				      	showMeridian: false,
				      	minuteStep: 1
					});
					    
					$("#btnGuardarHrsExtras").prop("disabled", false);
		        }
		        else if(tr.trim() === ''){

		        	// console.log("disabled", tr);
		        	$("#tblHorasExtras").DataTable().clear().destroy();
		        	$("#tblHorasExtras tbody").html(tr);
		        	$("#tblHorasExtras").DataTable({
	              		searching : false,
						paging : false,
				        scrollY: '60vh',
				      	scrollCollapse: true,
	              		scrollX: true,
	              		retrieve: true
	          		});

		        	$("#btnGuardarHrsExtras").prop("disabled", true);
		        }

	        	$(".loader").fadeOut("fast");
	        }
	    });
	});
}

function validarInfoGuardada(Datos)
{
	$.ajax(
  	{
		data: { datos:Datos },
	    url: 'crudHorario.php?accion=validarRepoHrsExtras',
	    type: 'post',
	    dataType: "json",
	    success: function (data) 
	    {

	    	if(data === null){

	    		$("#btnReporteHrsExtras").attr("style","display: none;");
	    		$("#btnExportarHrsExtras").attr("style","display: none;");
	    		$("#btnRevisado").attr("style","display: none;");
	    		$("#btnAutorizar").attr("style","display: none;");
	    		$("#btnGuardarHrsExtras").attr("style","");
		    }
		    else
		    {
		    	$("#btnReporteHrsExtras").attr("style","display: none;");
	    		$("#btnExportarHrsExtras").attr("style","display: none;");
	    		$("#btnRevisado").attr("style","display: none;");
	    		$("#btnAutorizar").attr("style","display: none;");
		    	$("#btnGuardarHrsExtras").attr("style","");
		    	
		    	if(data.revisado === "0")
		    	{
		    		$("#btnExportarHrsExtrasRH").attr("style","");
		    		$("#btnReporteHrsExtras").attr("style","");
		    		$("#btnRevisado").attr("style","");
		    	}
		    	if(data.revisado === "1")
		    	{
		    		$("#btnExportarHrsExtrasRH").attr("style","");
		    		$("#btnReporteHrsExtras").attr("style","");
		    		$("#btnAutorizar").attr("style","");
		    	}
		    	if(data.autorizado === "1")
		    	{
		    		$("#btnExportarHrsExtrasRH").attr("style","");
		    		$("#btnReporteHrsExtras").attr("style","");
		    		$("#btnExportarHrsExtras").attr("style","");
		    		$("#btnRevisado").attr("style","display: none;");
		    		$("#btnAutorizar").attr("style","display: none;");
		    		$("#btnGuardarHrsExtras").attr("style","display: none;");
		    	}
		    }
	    }

  	});

}

$(document).on("click", "#btnGuardarHrsExtras", function()
{
		let Datos = {};

		Datos["empresa"] = empresa;
		Datos["departamento"] = departamento;
		Datos["fechNomi"] = fechNomi;//$("#fechaNomina option:selected").val();
		Datos["cve_trabajador"] = [];
		Datos["nombre"] = [];
		Datos["asistenciaLun"] = [];
		Datos["hrsAproLun"] = [];
		Datos["asistenciaMar"] = [];
		Datos["hrsAproMar"] = [];
		Datos["asistenciaMie"] = [];
		Datos["hrsAproMie"] = [];
		Datos["asistenciaJue"] = [];
		Datos["hrsAproJue"] = [];
		Datos["asistenciaVie"] = [];
		Datos["hrsAproVie"] = [];
		Datos["asistenciaSab"] = [];
		Datos["hrsAproSab"] = [];
		Datos["asistenciaDom"] = [];
		Datos["hrsAproDom"] = [];
		Datos["hrsTotal"] = [];

		if ($( "input[aria-controls='tblHorasExtras']" ).val()) {			
			$('#tblHorasExtras').DataTable().destroy();
			$('#tblHorasExtras').DataTable({
				searching : true,
			  	paging : false,
			  	scrollY: '60vh',
				scrollCollapse: true,
				scrollX: true,
				retrieve: true
			}).draw();
		}
		
		$("#tblHorasExtras > tbody > tr").each(function(column, td){

			Datos["cve_trabajador"].push( $(this).find("td:eq(0)").text().trim() );
			Datos["nombre"].push( $(this).find("td:eq(1)").text() );
			Datos["asistenciaLun"].push( $(this).find("td:eq(2) .asistenciaLun option:selected").text() );
			Datos["hrsAproLun"].push( $(this).find("td:eq(4) .hrsAproLun").val() );
			Datos["asistenciaMar"].push( $(this).find("td:eq(5) .asistenciaMar option:selected").text() );
			Datos["hrsAproMar"].push( $(this).find("td:eq(7) .hrsAproMar").val() );
			Datos["asistenciaMie"].push( $(this).find("td:eq(8) .asistenciaMie option:selected").text() );
			Datos["hrsAproMie"].push( $(this).find("td:eq(10) .hrsAproMie").val() );
			Datos["asistenciaJue"].push( $(this).find("td:eq(11) .asistenciaJue option:selected").text() );
			Datos["hrsAproJue"].push( $(this).find("td:eq(13) .hrsAproJue").val() );
			Datos["asistenciaVie"].push( $(this).find("td:eq(14) .asistenciaVie option:selected").text() );
			Datos["hrsAproVie"].push( $(this).find("td:eq(16) .hrsAproVie").val() );
			Datos["asistenciaSab"].push( $(this).find("td:eq(17) .asistenciaSab option:selected").text() );
			Datos["hrsAproSab"].push( $(this).find("td:eq(19) .hrsAproSab").val() );
			Datos["asistenciaDom"].push( $(this).find("td:eq(20) .asistenciaDom option:selected").text() );
			Datos["hrsAproDom"].push( $(this).find("td:eq(22) .hrsAproDom").val() );
			Datos["hrsTotal"].push( $(this).find("td:eq(23) .hrsExtasTotal").val() );

		});

		if(Datos["cve_trabajador"].length > 0)
			guardarReporteHrsExtras(Datos);
});


$(document).on("click", ".asistenciaLun, .asistenciaMar, .asistenciaMie, .asistenciaJue, .asistenciaVie, .asistenciaSab, .asistenciaDom",function()
{
	let val = $(this).val();

	$(this).removeClass("bg-red");
	$(this).removeClass("bg-blue");
	$(this).removeClass("bg-yellow");
	$(this).removeClass("bg-orange");

	switch(val) {
		case "F":
	    	$(this).addClass("bg-red");
	    break;

		case "P":
	    	$(this).addClass("bg-blue");
	    break;

	    case "INC":
	    	$(this).addClass("bg-yellow");
	    break;

	    case "FJ":
	    	$(this).addClass("bg-orange");
	    break;
	    				
	}
		
});

function guardarReporteHrsExtras(Datos)
{
	$(".loader").fadeIn("slow", function(){

		$.ajax(
	    {
	        data:{datos:Datos},
	        url: 'crudHorario.php?accion=guardarReporteHrsExtras',
	        type: 'post',
	        success: function (data) 
	        {
	        	$(".loader").fadeOut("fast");

	        	if(data.trim() === "0")
	            {
	                swal(
		                'Reporte de Hrs Extras',
		                '¡No se realizó ninguna modificación!',
		                'warning'
		            )
	            }
	            else if(data.trim() === "1")
	            {
	            	swal(
		                'Reporte de Hrs Extras',
		                '¡Los datos se agregaron correctamente!',
		                'success'
		            )
		            validarInfoGuardada(Datos)
		            $("#btnRevisado").attr("style","");
	            }
	            else if(data.trim() === "2")
	            {
	            	swal(
		                'Reporte de Hrs Extras',
		                '¡Los datos se modificaron correctamente!',
		                'success'
		            )
		            validarInfoGuardada(Datos)
	            }
	            else
	            {
                	swal(
		                '¡Reporte de Hrs Extras!',
		                data.trim(),
		                'warning'
		            )
	            }
	        }
	    });
	});
}

$(document).on("click", "#btnReporteHrsExtras", function(){

	let Datos = {};
	Datos["empresa"] = empresa;
	Datos['departamento'] = departamento;
	Datos["fechNomi"] = fechNomi;

	if(Datos['empresa'] != undefined && Datos['departamento'] != undefined && Datos['fechNomi'] != undefined)
		generarPdfHrsExtras(Datos);

});

function generarPdfHrsExtras(Datos)
{
	$.ajax(
  	{
    	data:{datos:Datos},
      	url: 'crudHorario.php?accion=reporteHrsExtras',
      	type: 'post',
      	success: function (data) 
      	{
    		if(data.trim() === "Sin autorizar")
	    	{
		    	swal(
		            'Para realizar el formato PDF',
		            '¡Los datos deben estar autorizados!',
		            'warning'
		        )
	    	}
	    	else
	      		window.open("crudHorario.php?accion=generarPdfHrsExtras&dep="+departamentoText, 'TheWindow',"toolbar=yes,scrollbars=yes,resizable=yes,top=50,left=100,width=1700,height=900");

      	}
  	});
}

$(document).on("click","#btnExportarHrsExtrasRH", function(){

	let Datos = {};
	Datos['empresa'] = empresa;
	Datos['departamento'] = departamento;
	Datos['fechNomi'] = fechNomi;

	if(Datos['empresa'] != undefined && Datos['departamento'] != undefined && Datos['fechNomi'] != undefined)
		exportarHrsExtrasRH(Datos)

});

function exportarHrsExtrasRH(Datos)
{
	$.ajax(
  	{
    	data:{datos:Datos},
    	url: 'crudHorario.php?accion=reporteHrsExtras',
    	type: 'post',
    	success: function (data) 
    	{
    		if(data.trim() === "Sin autorizar")
    		{
	    			swal(
		            'Para realizar la exportación de los datos',
		            '¡Los datos deben estar autorizados!',
		            'warning'
		        )
	    	}
	    	else
				$("body").append("<iframe src='crudHorario.php?accion=exportarHrsExtrasRH' style='display: none;' ></iframe>");
		}
  	});
}

$(document).on("click","#btnExportarHrsExtras", function(){

	let Datos = {};
	Datos['empresa'] = empresa;
	Datos['departamento'] = departamento;
	Datos['fechNomi'] = fechNomi;

	if(Datos['empresa'] != undefined && Datos['departamento'] != undefined && Datos['fechNomi'] != undefined)
		exportarHrsExtras(Datos)

});

function exportarHrsExtras(Datos)
{
	$.ajax(
  	{
    	data:{datos:Datos},
    	url: 'crudHorario.php?accion=reporteHrsExtrasExcel',
    	type: 'post',
    	success: function (data) 
    	{
    		if(data.trim() === "Sin autorizar")
    		{
	    			swal(
		            'Para realizar la exportación de los datos',
		            '¡Los datos deben estar autorizados!',
		            'warning'
		        )
	    	}
	    	else
				$("body").append("<iframe src='crudHorario.php?accion=exportarHrsExtras' style='display: none;' ></iframe>");
		}
  	});
}

$(document).on("click","#btnRevisado", function()
{
	let Datos = {};
	Datos["empresa"] = empresa;
	Datos["departamento"] = departamento;
	Datos["departamentoText"] = departamentoText;
	Datos["fechNomi"] = fechNomi;
	
	accionRevisarAutorizar(Datos, 'revisarRepoHrsExtras');

});

$(document).on("click","#btnAutorizar", function()
{
	let Datos = {};
	Datos["empresa"] = empresa;
	Datos["departamento"] = departamento;
	Datos["departamentoText"] = departamentoText;
	Datos["fechNomi"] = fechNomi;
	accionRevisarAutorizar(Datos, 'autorizarRepoHrsExtras');

});

function accionRevisarAutorizar(Datos, rute)
{
	$(".loader").fadeIn("slow", function(){
		$.ajax(
	  {
	      data:{datos:Datos},
	      url: 'crudHorario.php?accion='+rute,
	      type: 'post',
	      success: function (data) 
	      {
	      	$(".loader").fadeOut("fast");
	      	
	      	if(data.trim() === "No hay datos guardados")
	      	{
	      		swal(
		          'Reporte de Hrs Extras',
		          '¡Los datos aun no se guardan, para realizar esta acción!',
		          'warning'
	          )
	      	}
	      	else
	      	{
	      		if(data.trim() === "0")
		        {
		          swal(
		            'Reporte de Hrs Extras',
		            '¡No se realizó ninguna acción!',
		            'warning'
		          )
		        }
		        else if(data.trim() === "1")
		        {
		        	if(rute === 'revisarRepoHrsExtras'){

			        	swal(
			            'Reporte de Hrs Extras',
			            '¡Los datos se revisaron correctamente!',
			            'success'
			          )
			          
			        	$("#btnReporteHrsExtras").attr("style","display: none;");
	    				$("#btnExportarHrsExtras").attr("style","display: none;");
			        	$("#btnRevisado").attr("style","display: none;");
		    			$("#btnAutorizar").attr("style","");

			        }
			        else
			        {
			        	swal(
				        	'Reporte de Hrs Extras',
				            '¡Los datos fueron autorizados correctamente!',
				            'success'
				        )

			          	$("#btnReporteHrsExtras").attr("style","");
			    		$("#btnExportarHrsExtras").attr("style","");
			    		$("#btnRevisado").attr("style","display: none;");
			    		$("#btnAutorizar").attr("style","display: none;");
			    		$("#btnGuardarHrsExtras").attr("style","display: none;");
			        }
		        }
		        else
		        {
		        	swal(
		            	'Reporte de Hrs Extras',
		              	data.trim(),
		              	'warning'
		            )
		        }
	      	}

	      }
	  });
	});
}

$(document).on("change keyup", ".hrsAproLun, .hrsAproMar, .hrsAproMie, .hrsAproJue, .hrsAproVie, .hrsAproSab, .hrsAproDom", function()
{
	let fila = $(this).closest('tr')
	let hrsLun = fila.find("td:eq(4) .hrsAproLun").val();
	let hrsMar = fila.find("td:eq(7) .hrsAproMar").val();
	let hrsMie = fila.find("td:eq(10) .hrsAproMie").val();
	let hrsJue = fila.find("td:eq(13) .hrsAproJue").val();
	let hrsVie = fila.find("td:eq(16) .hrsAproVie").val();
	let hrsSab = fila.find("td:eq(19) .hrsAproSab").val();
	let hrsDom = fila.find("td:eq(22) .hrsAproDom").val();
	
	let hrs = [hrsLun, hrsMar, hrsMie, hrsJue, hrsVie, hrsSab, hrsDom];
	convertir(fila, hrs)
	

});

function convertir(fila, hrs){

	let total = 0;

	hrs.forEach(function(time, index) {
		// console.log("time", time);
		if(time != 0)
		{
			let parts = time.split(':');

			var horas = parts[0];
			var minutos =  parts[1];
			 
			horas = parseFloat(horas);
			minutos = parseFloat(minutos);
			 
			total += minutos * 60 + horas * 3600;
			// console.log("total", total);
		}
		
	});
 	
 	let result = secondsToString(total);

 	if(result != 'NaN:NaN')
 		fila.find("td:eq(23) .hrsExtasTotal").val(result);
 	// console.log("total", result);
}

function secondsToString(seconds) {

  var hour = Math.floor(seconds / 3600);
  hour = (hour < 10)? '0' + hour : hour;
  var minute = Math.floor((seconds / 60) % 60);
  minute = (minute < 10)? '0' + minute : minute;
  // var second = seconds % 60;
  // second = (second < 10)? '0' + second : second;
  return hour + ':' + minute; //+ ':' + second;
}

function preload()
{
    let timerInterval
    swal({
        type: "warning",
        title: "Session finalizada",
        text: "¡Redireccionando a página login...!",
        showConfirmButton: false,
        timer: 3000,
        onBeforeOpen: () => {
          swal.showLoading()
          timerInterval = setInterval(() => {
          const content = Swal.getContent()
            if (content) {
                const b = content.querySelector("b")
                if (b) {
                    b.textContent = Swal.getTimerLeft()
                }
            }
          }, 100)
        },
        onClose: () => {
          clearInterval(timerInterval)
        }
    });
        
        setTimeout(ingresoWeb, 3000);


}

$(document).on("change", "#empresaPD", function()
{
	let empresa = $(this).val();
	if(!isEmpty(empresa))
		cargarPeriodoNominaPD(empresa)

});

function cargarPeriodoNominaPD(empresa)
{
	let Datos = {};
	Datos['empresa'] = empresa;
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominal',
	      	type: 'post',
	      	success: function (option) 
	      	{

        		if(option.trim() != 'No existen datos')
        		{
    				$("#fechaNominaPD").find('option').remove().end().append(option);
    				Datos['fechNomi'] = $("#fechaNominaPD option:selected").val();
      				$('#fechaNominaPD').val(Datos['fechNomi']).selectpicker('refresh');
      				$(".loader").fadeOut("fast");
      				cargartblPersonalPrimaDominical(Datos);
        		}
        		else
        		{
        			$(".loader").fadeOut("fast");
    				$("#fechaNominaPD").html("");
    				$('#fechaNominaPD').val(null).selectpicker('refresh');
        		}
        		
        		
	      	}
	    });
	});
}

$(document).on("change", "#fechaNominaPD", function()
{
		let Datos = {};
		Datos['empresa'] = $("#empresaPD option:selected").val();
		Datos['fechNomi'] = $(this).val(); 
		cargartblPersonalPrimaDominical(Datos);

});

function cargartblPersonalPrimaDominical(Datos)
{
	$(".loader").fadeIn("slow", function()
  	{
	    $.ajax(
	    {
		    data:{ datos:Datos },
	        url: 'crudHorario.php?accion=tblPersonalPrimaDominical',
	        type: 'post',
	        success: function (tr) 
	        {
	        	// console.log("tr", tr);
		        $("#tblPersonalPD").DataTable().clear().destroy();
		        $("#tblPersonalPD tbody").html(tr);
				$("#tblPersonalPD").DataTable({
					searching : false,
					paging : false,
			        scrollY: '60vh',
			      	scrollCollapse: true,
			        // order: [[ 1, "asc" ], [ 3, 'asc' ]],
			        retrieve: true
		       	});

		       	let countCheckPD = $("input.checkPersonalPD:checked").length;
				let countCheckAT = $("input.checkPersonalAT:checked").length;

				$("#btnGuardarRegPD").prop("disabled", true);
				if(countCheckPD > 0 || countCheckAT > 0)
					$("#btnGuardarRegPD").prop("disabled", false);

				validarRepoPDGuardada(Datos)

          		$(".loader").fadeOut("fast");

        	}
	    });
	});

}

$(document).on("click", "input.checkPersonalPD, input.checkPersonalAT", function(){

	let countCheckPD = $("input.checkPersonalPD:checked").length;
	let countCheckAT = $("input.checkPersonalAT:checked").length;

	$("#btnGuardarRegPD").prop("disabled", true);
	if(countCheckPD > 0 || countCheckAT > 0)
		$("#btnGuardarRegPD").prop("disabled", false);

});

$(document).on("click", "#btnGuardarRegPD", function()
{
	let Datos = {};
	let countCheckPD = $("input.checkPersonalPD:checked").length;
	let countCheckAT = $("input.checkPersonalAT:checked").length;
	Datos['empresa'] = $('#empresaPD option:selected').val();
	Datos['fechNomi'] = $('#fechaNominaPD option:selected').val();
	Datos['PD'] = [];
	Datos['AT'] = [];
	Datos['cve_trabajador'] = [];

  	$("#tblPersonalPD > tbody > tr").each(function(){

		let checkNumPD = $(this).find('td:eq(0) input[type="checkbox"]');
		let checkNumAT = $(this).find('td:eq(1) input[type="checkbox"]');

		if(checkNumPD.is(':checked') || checkNumAT.is(':checked'))
		{
			Datos['PD'].push( checkNumPD.is(':checked') );
			Datos['AT'].push( checkNumAT.is(':checked') );
			Datos['cve_trabajador'].push( $(this).find("td:eq(3)").text() );
		}

  	});
		
	if((countCheckPD > 0 || countCheckAT > 0) && Datos['cve_trabajador'].length > 0)
		guardarRepPrimaDominical(Datos);
});

function guardarRepPrimaDominical(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ datos:Datos },
	        url: 'crudHorario.php?accion=guardarRepPrimaDominical',
	        type: 'post',
	        success: function (data) 
	        {
	            if(data.trim() == '0')
	            {
	                swal(
		                'Prima Dominical',
		                '¡No se realizó ninguna modificación!',
		                'warning'
		            )
	            }
	            else if(data.trim() == '1')
	            {
	            	validarRepoPDGuardada(Datos)
	                swal(
	                    '¡Alta de registro!',
	                    'Se agrego correctamente',
	                    'success'
	                )
	            }
	            else
	            {
	            	swal(
	                    '¡No se puede realizar alta de registro!',
	                    data,
	                    'warning'
	                )
	            }
	            $(".loader").fadeOut("fast");

	        }
	    });
	});
}

function validarRepoPDGuardada(Datos)
{
	$.ajax(
  	{
		data: { datos:Datos },
	    url: 'crudHorario.php?accion=validarRepoPD',
	    type: 'post',
	    success: function (data) 
	    {
	    	$("#btnExportarPD").attr("style","display: none;");
	    	if(data > 0)
	    		$("#btnExportarPD").attr("style","");
	    }

  	});

}

$(document).on("click","#btnExportarPD", function(){

	let Datos = {};
	Datos['empresa'] = $("#empresaPD option:selected").val();
	Datos['fechNomi'] = $("#fechaNominaPD option:selected").val();

	if(!isEmpty(Datos['empresa']) && !isEmpty(Datos['fechNomi']))
		exportarPD(Datos)

});

function exportarPD(Datos)
{
	$.ajax(
  	{
    	data:{datos:Datos},
    	url: 'crudHorario.php?accion=reportePDExcel',
    	type: 'post',
    	success: function (data) 
    	{
    		if(data.trim() === "¡No existen datos!")
    		{
	    		swal(
		            'Para realizar la exportación de los datos',
		            '¡Los datos deben estar guardados anteriormente!',
		            'warning'
		        )
	    	}
	    	else
				$("body").append("<iframe src='crudHorario.php?accion=exportarPD' style='display: none;' ></iframe>");
		}
  	});
}

$(document).on("change", "#empresaPP", function()
{
	let empresa = $(this).val();
	if(!isEmpty(empresa))
		cargarPeriodoNominaPP(empresa)

});

function cargarPeriodoNominaPP(empresa)
{
	let Datos = {};
	Datos['empresa'] = empresa;
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominalAsignacionGrupoHorario',
	      	type: 'post',
	      	success: function (option) 
	      	{
	      		$(".loader").fadeOut("fast");

        		if(option.trim() != 'No existen datos')
        		{
        			$('#empresaPP').val(empresa).selectpicker('refresh');
    				$("#fechaNominaPP").find('option').remove().end().append(option);
    				Datos['fechNomi'] = $("#fechaNominaPP option:selected").val();
      				$('#fechaNominaPP').val(Datos['fechNomi']).selectpicker('refresh');
      				// cargartblPersonalPremioPuntualidad(Datos);
        		}
        		else
        		{
    				$("#fechaNominaPP").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaPP').val(null).selectpicker('refresh');
    				cleanTblPersonalPP()
        		}
        		
	      	}
	    });
	});
}

function cleanTblPersonalPP()
{
	$("#tblPersonalPP").DataTable().clear().destroy();
    $("#tblPersonalPP tbody").html(null);
	$("#tblPersonalPP").DataTable({
		searching : false,
		paging : false,
        scrollY: '60vh',
        scrollX: true,
      	scrollCollapse: true,
        order: [[ 1, "asc" ], [ 3, 'asc' ]],
        retrieve: true
   	});
}

$(document).on("change", "#fechaNominaPP", function()
{
	let Datos = {};
	Datos['empresa'] = $("#empresaPP option:selected").val();
	Datos['fechNomi'] = $(this).val(); 
	cargartblPersonalPremioPuntualidad(Datos);

});

function cargartblPersonalPremioPuntualidad(Datos)
{
	$(".loader").fadeIn("slow", function()
  	{
	    $.ajax(
	    {
		    data:{ datos:Datos },
	        url: 'crudHorario.php?accion=tblPersonalPremioPuntualidad',
	        type: 'post',
	        success: function (tr) 
	        {
	        	// console.log("tr", tr);
		        $("#tblPersonalPP").DataTable().clear().destroy();
		        $("#tblPersonalPP tbody").html(tr);
				$("#tblPersonalPP").DataTable({
					searching : false,
					paging : false,
			        scrollY: '60vh',
			        scrollX: true,
			      	scrollCollapse: true,
			        order: [[ 1, "asc" ], [ 3, 'asc' ]],
			        retrieve: true
		       	});

		       	let rowCount = $('#tblPersonalPP tbody tr').length;
		       	// console.log("rowCount", rowCount);

            	$("#btnGuardarRegPP").prop('disabled', true);
            	if(rowCount > 0)
            		$("#btnGuardarRegPP").prop('disabled', false);

            	validarRepoPPGuardada(Datos);

          		$(".loader").fadeOut("fast");

        	}
	    });
	});

}

$(document).on("click", "#btnGuardarRegPP", function()
{
	let Datos = {};

	Datos['empresa'] = $('#empresaPP option:selected').val();
	Datos['fechNomi'] = $('#fechaNominaPP option:selected').val();
	Datos['pp'] = [];
	Datos['cve_trabajador'] = [];

  	$("#tblPersonalPP > tbody > tr").each(function(){

  		let pp = $(this).find('td:eq(1) .PP').val();

  		if(pp != undefined){
			Datos['pp'].push( pp );
			Datos['cve_trabajador'].push( $(this).find("td:eq(2)").text() );
		}

  	});
		
	if(Datos['cve_trabajador'].length > 0 && !isEmpty(Datos['empresa']) && !isEmpty(Datos['fechNomi']))
		guardarRepPremioPuntualidad(Datos);
});

function guardarRepPremioPuntualidad(Datos)
{
	$(".loader").fadeIn("slow", function()
    {
	    $.ajax(
	    {
	    	data:{ datos:Datos },
	        url: 'crudHorario.php?accion=guardarRepPremioPuntualidad',
	        type: 'post',
	        success: function (data) 
	        {
	            if(data.trim() == '0')
	            {
	                swal(
		                'Premio de Puntualidad',
		                '¡No se realizó ninguna modificación!',
		                'warning'
		            )
	            }
	            else if(data.trim() == '1')
	            {
	            	validarRepoPPGuardada(Datos)
	                swal(
	                    '¡Alta de registro!',
	                    'Se agrego correctamente',
	                    'success'
	                )
	            }
	            else
	            {
	            	swal(
	                    '¡No se puede realizar alta de registro!',
	                    data,
	                    'warning'
	                )
	            }
	            $(".loader").fadeOut("fast");

	        }
	    });
	});
}

function validarRepoPPGuardada(Datos)
{
	$.ajax(
  	{
		data: { datos:Datos },
	    url: 'crudHorario.php?accion=validarRepoPP',
	    type: 'post',
	    success: function (data) 
	    {
	    	$("#btnExportarPP").attr("style","display: none;");
	    	if(data > 0)
	    		$("#btnExportarPP").attr("style","");
	    }

  	});

}

$(document).on("click","#btnExportarPP", function(){

	let Datos = {};
	Datos['empresa'] = $("#empresaPP option:selected").val();
	Datos['fechNomi'] = $("#fechaNominaPP option:selected").val();

	if(!isEmpty(Datos['empresa']) && !isEmpty(Datos['fechNomi']))
		exportarPP(Datos)

});

function exportarPP(Datos)
{
	$.ajax(
  	{
    	data:{datos:Datos},
    	url: 'crudHorario.php?accion=reportePPExcel',
    	type: 'post',
    	success: function (data) 
    	{
    		if(data.trim() === "¡No existen datos!")
    		{
	    		swal(
		            'Para realizar la exportación de los datos',
		            '¡Los datos deben estar guardados anteriormente!',
		            'warning'
		        )
	    	}
	    	else
				$("body").append("<iframe src='crudHorario.php?accion=exportarPP' style='display: none;' ></iframe>");
		}
  	});
}
// function ingresoWeb()
// {
//     window.location.href = "../view/index.php";
// }

$(document).on("change", "#empresaRF", function()
{
	let empresa = $(this).val();
	if(!isEmpty(empresa))
		cargarPeriodoNominaRF(empresa)

});

function cargarPeriodoNominaRF(empresa)
{
	let Datos = {};
	Datos['empresa'] = empresa;
	$(".loader").fadeIn("slow", function()
  	{
		$.ajax(
	    {
	    	data:{empresa:empresa},
	      	url: 'crudHorario.php?accion=periodoNominalAsignacionGrupoHorario',
	      	type: 'post',
	      	success: function (option) 
	      	{
	      		$(".loader").fadeOut("fast");

        		if(option.trim() != 'No existen datos')
        		{
        			$('#empresaRF').val(empresa).selectpicker('refresh');
    				$("#fechaNominaRF").find('option').remove().end().append(option);
    				Datos['fechNomi'] = $("#fechaNominaRF option:selected").val();
      				$('#fechaNominaRF').val(Datos['fechNomi']).selectpicker('refresh');
      				cargarTblPersonalRegistroFaltas(Datos);
        		}
        		else
        		{
    				$("#fechaNominaRF").find('option').remove().end().append('<option></option>');
    				$('#fechaNominaRF').val(null).selectpicker('refresh');
    				cleanTblPersonalRegistroFaltas()
        		}
        		
	      	}
	    });
	});
}

$(document).on("change", "#fechaNominaRF", function()
{
		let Datos = {};
		Datos['empresa'] = $("#empresaPP option:selected").val();
		Datos['fechNomi'] = $(this).val(); 
		cargarTblPersonalRegistroFaltas(Datos);

});

function cleanTblPersonalRegistroFaltas()
{
	$("#tblPersonalRF").DataTable().clear().destroy();
    $("#tblPersonalRF tbody").html(null);
	$("#tblPersonalRF").DataTable({
		order: [[ 0, "asc" ], [ 3, 'asc' ]],
        scrollY: '60vh',
        scrollX: true,
  		scrollCollapse: true,
  		retrieve: true
	}).columns.adjust();
}

function cargarTblPersonalRegistroFaltas(Datos)
{
	// $(".loader").fadeIn("slow", function()
 //  	{
 	// $("#loadTblRf").LoadingOverlay("show");
 	$("#loadTblRf").LoadingOverlay("show", {
	    image       : "../img/logos/logo-icon.png",
	    imageAnimation  : "1.5s fadein"
	});

    $.ajax(
    {
	    data:{ datos:Datos },
        url: 'crudHorario.php?accion=tblPersonalRegitrosFaltas',
        type: 'post',
        success: function (tr) 
        {
      //   	validarRepoPPGuardada(Datos);
      		let count     = 0;
      		let interval  = setInterval(function(){
	    		if (count >= 40) {
	    			clearInterval(interval);
		      		$("#tblPersonalRF").DataTable().clear().destroy();
			        $("#tblPersonalRF tbody").html(tr);
			        $("#tblPersonalRF").DataTable({
			        	// pagingType: 'full_numbers',
			        	columnDefs : [{
					        targets: [0,2,3,4,5,6,7,8,9,10],
					        className: 'text-center'
					    }],
						order: [[ 0, "asc" ], [ 3, 'asc' ]],
			            scrollY: '60vh',
			            scrollX: true,
			            // paging: false,
			      		scrollCollapse: true,
		          		retrieve: true
		       		}).columns.adjust();

			       	let rowCount = $('#tblPersonalRF tbody tr.RF').length;

		        	$("#btnExportarRF").prop('disabled', true);
		        	$("#btnGuardarRF").prop('disabled', true);
		        	if(rowCount > 0)
		        	{
		        		$("#btnExportarRF").prop('disabled', false);
		        		$("#btnGuardarRF").prop('disabled', false);
		        	}

		        	$("#loadTblRf").LoadingOverlay("hide", true);

		        }
		        count += 10;
	        }, 300);

    	}
    });
	// });

}



$('#tblPersonalRF').on('draw.dt', function () { 
    $(".fechaInc").datepicker({
  		format: 'dd-mm-yyyy',
        autoclose: true,
        language: 'es'
	});
});


$(document).on("click", "#btnGuardarRF", function(){
		
	let Datos = {};

	Datos["empresa"] = $("#empresaRF option:selected").val();
	Datos["fechNomi"] = $("#fechaNominaRF option:selected").val();
	Datos["id_row"] = [];
	Datos["cve_trabajador"] = [];
	Datos["fechaAusencia"] = [];
	Datos["fInicial"] = [];
	Datos["fFinal"] = [];
	Datos["tipoFalta"] = [];
	Datos["certificadoIMSS"] = [];
	Datos["tipoIncapacidad"] = [];
	Datos["prcFalta"] = [];

	var table = $('#tblPersonalRF').DataTable();

    table.rows().every( function ( value, index ) {
    	
    	var row = $(this.node());

  		let id_row = row.attr('id-row');
  	    let cve_trabajador = row.find("td:eq(0)").text();
      	let fechaAusencia = row.find("td:eq(3)").text();
      	let fInicial = row.find("td:eq(4) input[type='text']").val();
      	fInicial = fInicial === undefined ? '' : fInicial;
      	let fFinal = row.find("td:eq(5) input[type='text']").val();
      	fFinal = fFinal === undefined ? '' : fFinal;
      	let tipoFalta = row.find("td:eq(6)").text();
      	let CIMSS = (tipoFalta == 1) ? row.find("td:eq(7) .CIMSS").val() : '';
      	CIMSS = CIMSS === undefined ? '' : CIMSS;
      	let tipoInc = (tipoFalta == 1) ? row.find("td:eq(8) .INC option:selected").val() : '';
      	tipoInc = tipoInc === undefined ? '' : tipoInc;
      	let prcFalta = row.find("td:eq(9) input[type='text']").val();
      	prcFalta = prcFalta === undefined ? '' : prcFalta;

      	Datos["id_row"].push(id_row);
      	Datos["cve_trabajador"].push(cve_trabajador);
		Datos["fechaAusencia"].push(fechaAusencia);
		Datos["fInicial"].push(fInicial);
		Datos["fFinal"].push(fFinal);
		Datos["tipoFalta"].push(tipoFalta);
		Datos["certificadoIMSS"].push(CIMSS);
		Datos["tipoIncapacidad"].push(tipoInc);
		Datos["prcFalta"].push(prcFalta);

    } );

	if(Datos["cve_trabajador"].length > 0)
      	guardarRegitroFaltas(Datos);

    // console.log("Datos", Datos);

});

function guardarRegitroFaltas(Datos)
{
	// $(".loader").fadeIn("slow", function(){

		$.ajax(
	    {
	        data:{datos: JSON.stringify(Datos)},
	        url: 'crudHorario.php?accion=guardarRegitroFaltas',
	        type: 'post',
	        success: function (data) 
	        {
	        	// $(".loader").fadeOut("fast");
	        	let tblLoad = {};
	        	tblLoad['empresa'] = Datos['empresa'];
	        	tblLoad['fechNomi'] = Datos['fechNomi'];
	        	cargarTblPersonalRegistroFaltas(tblLoad)

	        	if(data.trim() === "0")
	            {
	                swal(
		                'Reporte de Incapacidades y faltas',
		                '¡No se realizó ninguna modificación!',
		                'warning'
		            )
	            }
	            else if(data.trim() === "1")
	            {
	            	swal(
		                'Reporte de Incapacidades y faltas',
		                '¡Los datos se agregaron correctamente!',
		                'success'
		            )
		            // validarInfoGuardada(Datos)
		            // $("#btnRevisado").attr("style","");
	            }
	            else if(data.trim() === "2")
	            {
	            	swal(
		                'Reporte de Incapacidades y faltas',
		                '¡Los datos se modificaron correctamente!',
		                'success'
		            )
		            // validarInfoGuardada(Datos)
	            }
	            else
	            {
                	swal(
		                '¡Reporte de Incapacidades y faltas!',
		                data.trim(),
		                'warning'
		            )
	            }
	        }
	    });
	// });
}

$(document).on("click","#btnExportarRF", function(){

	let Datos = {};
	Datos['empresa'] = $("#empresaRF option:selected").val();
	Datos['fechNomi'] = $("#fechaNominaRF option:selected").val();

	if(!isEmpty(Datos['empresa']) && !isEmpty(Datos['fechNomi']))
		$("body").append("<iframe src='crudHorario.php?accion=exportarRF&empresa="+Datos['empresa']+"&fechNomi="+Datos['fechNomi']+"' style='display: none;' ></iframe>");
		// $(this).prop('disabled', true);

});

$(document).on("keypress",".allow_decimal", function(evt) {
	var self = $(this);
    self.val(self.val().replace(/[^0-9\.]/g, ''));
    if ((evt.which != 46 || self.val().indexOf('.') != -1) && (evt.which < 48 || evt.which > 57)) 
    {
    	evt.preventDefault();
   	}
 });


$("#tblPersonalRF tbody").on( 'click', '.btnCopy', function () {
    
    let d = $('#tblPersonalRF').DataTable().row( $(this).parents('tr') ).data();
      
   $('#tblPersonalRF').DataTable().row.add(d).draw(false).columns.adjust();
   // $(row).css( 'color', 'red' ).animate( { color: 'black' } );

});

$.ajaxSetup({
    error: function( jqXHR, textStatus, errorThrown ) {
    
    $(".loader").fadeOut("fast");

    if (jqXHR.status === 0) {

        swal({
            title: "Error",
            text: "¡No hay Conexion con el servidor, favor de revisar la RED!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });


      } else if (jqXHR.status == 404) {

        swal({
            title: "Error [404]",
            text: "¡La operacion no se realizó!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

      } else if (jqXHR.status == 500) {

        swal({
            title: "Error [500]",
            text: "¡Error interno del servidor!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

      } else if (textStatus === 'parsererror') {

        swal({
            title: "Error JSON",
            text: "¡El análisis JSON solicitado falló!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

      } else if (textStatus === 'timeout') {

        swal({
            title: "Error",
            text: "¡Error de tiempo de espera agotado!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });


      } else if (textStatus === 'abort') {

        swal({
            title: "Error Ajax",
            text: "¡Solicitud de Ajax abortada!",
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

      } else {

        swal({
            title: "Error",
            text: "¡Error no detectado!" + jqXHR.responseText,
            type: "error",
            confirmButtonText: "¡Cerrar!"
        });

      }

      setTimeout(ingresoWeb, 5000);
      $("#loadTblRf").LoadingOverlay("hide", true);
    }
});

function ingresoWeb()
{
	window.location = "../view/index.php";
}