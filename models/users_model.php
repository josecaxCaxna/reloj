<?php
    require_once(__DIR__.'/../config/dbConfig.php');
    /**
    * Usuarios
    */
    class users_model
    {
        protected $connection;
        function __construct()
        {
            $db = new dbConnection(); 
            $this->connection = $db->db();
        }

        public function executeQuery($query){
            $resultado = $this->connection->query($query);
            if($resultado){
                $nFilas = $resultado->num_rows;
                if($nFilas > 0){
                    while($fila = $resultado->fetch_assoc())
                        $dataOut[] = $fila;
                    return $dataOut;
                }else
                    return ["data"=>"empty"];
            }else
                return $this->connection->error;
        }
        
        public function listarUsuarios()
        {
            $sql = "SELECT fp_usuario.id,fp_usuario.email,fp_usuario.nombre,fp_usuario.estatus,fp_usuario.ultimo_acceso,fp_usuario.id_perfil,fp_empresa.id 'id_empresa',fp_empresa.nombre 'nombre_empresa' FROM fp_usuario INNER JOIN fp_empresa ON fp_usuario.id_empresa = fp_empresa.id";
            $resultado = $this->executeQuery($sql);            
            if ($resultado) {
                return $resultado;
            }
        }

        public function editarUsuario()
        {
            $id_user = $_POST["id_user"];
            $sql = "SELECT nombre,estatus,id_perfil FROM fp_usuario WHERE id = $id_user;";
            $resultado = $this->executeQuery($sql);
            if ($resultado) {
                return $resultado;
            }
        }
    }
?>