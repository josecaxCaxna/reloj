/*
SQLyog Ultimate
MySQL - 8.0.30 : Database - reloj
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Data for the table `fp_acciones` */

insert  into `fp_acciones`(`id_accion`,`descripcion`) values 
(1,'Visualizar'),
(2,'Generar'),
(3,'Exportar'),
(4,'Modificar'),
(5,'Importar'),
(6,'Revisar'),
(7,'Autorizar');

/*Data for the table `fp_asistencia` */

insert  into `fp_asistencia`(`id`,`id_r`,`id_empresa`,`cve_trabajador`,`fecha`,`tiempo`,`tipo_acceso`,`created_at`,`updated_at`) values 
(1,1,1,'3737','2022-09-27','09:00:00','ENTRADA','2022-09-27 11:59:14','2022-09-27 11:59:14'),
(2,1,1,'3737','2022-09-27','09:00:00','SALIDA','2022-09-27 11:59:14','2022-09-27 11:59:14'),
(3,1,1,'3737','2022-09-27','09:00:00','ENTRADA','2022-09-27 11:59:57','2022-09-27 11:59:57'),
(4,1,1,'3737','2022-09-27','09:00:00','SALIDA','2022-09-27 11:59:57','2022-09-27 11:59:57'),
(5,1,1,'3737','2022-09-27','09:00:00','ENTRADA','2022-09-27 12:00:26','2022-09-27 12:00:26'),
(6,1,1,'3737','2022-09-27','09:00:00','SALIDA','2022-09-27 12:00:26','2022-09-27 12:00:26'),
(7,1,1,'3737','2022-09-27','09:00:00','SALIDA','2022-09-27 12:00:26','2022-09-27 12:00:26'),
(8,1,1,'3737','2022-09-27','09:00:00','SALIDA','2022-09-27 12:00:26','2022-09-27 12:00:26'),
(9,1,1,'3737','2022-09-27','09:00:00','SALIDA','2022-09-27 12:00:26','2022-09-27 12:00:26');

/*Data for the table `fp_cat_mod_usu` */

insert  into `fp_cat_mod_usu`(`id`,`id_categoria`,`id_modulo`) values 
(1,2,1),
(2,1,2),
(3,2,3),
(4,3,4),
(5,3,5);

/*Data for the table `fp_categorias` */

insert  into `fp_categorias`(`id_categoria`,`nombre_categoria`) values 
(1,'Dashboard'),
(2,'Usuarios'),
(3,'Control Horario');

/*Data for the table `fp_empresa` */

insert  into `fp_empresa`(`id`,`nombre`,`directorio_db`,`id_user`,`created_at`,`updated_at`) values 
(1,'EMPRESA_PRUEBA','C:\\\\PROGRAM FILES (X86)\\\\COMMON FILES\\\\ASPEL\\\\Sistemas Aspel\\\\NOI9.00\\Datos\\\\Empresa',1,'2022-03-14 21:54:51','2022-03-14 21:54:51');

/*Data for the table `fp_modulos` */

insert  into `fp_modulos`(`id_modulo`,`nombre`,`alias`,`ruta`) values 
(1,'Listar Usuarios','users','listarUsuarios'),
(2,'Listar Empresas','index','index'),
(4,'Listar Grupos','horario','grupos'),
(5,'Listar Horarios','horario','horarios');

/*Data for the table `fp_perfiles` */

insert  into `fp_perfiles`(`id`,`nombre`) values 
(1,'ADMINISTRADOR'),
(2,'SUPERVISOR'),
(3,'VISITANTE');

/*Data for the table `fp_permiso_usuario_modulo` */

insert  into `fp_permiso_usuario_modulo`(`id`,`id_usuario`,`id_accion`,`id_modulo`) values 
(1,1,1,1),
(2,1,2,1),
(3,1,3,1);

/*Data for the table `fp_trabajador` */

insert  into `fp_trabajador`(`id`,`cve_trabajador`,`nombre_trabajador`,`ap_trabajador`,`am_trabajador`,`huella`,`directorio_img`,`id_empresa`,`estatus`,`created_at`,`updated_at`) values 
(1,'2236','Rafael','Sanabria','Santana',NULL,NULL,1,'1','2022-03-14 22:54:41','2022-03-14 22:54:41');

/*Data for the table `fp_usuario` */

insert  into `fp_usuario`(`id`,`email`,`contraseña`,`nombre`,`estatus`,`ultimo_acceso`,`id_empresa`,`id_perfil`,`created_at`,`updated_at`) values 
(1,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','Rafael Sanabria','1','0000-00-00 00:00:00',1,1,'2022-03-14 22:01:50','2022-09-06 22:04:39'),
(2,'prueba','8e1e27ec20a874faa8f82caefe49e9ad51e4a401','Prueba','1','2022-10-03 20:43:44',1,1,'2022-10-03 20:43:51','2022-10-06 10:04:50');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
