<?php
require_once(__DIR__.'/../config/dbConfig.php');
use Firebase\JWT\JWT;
  require_once './php-jwt-master/src/JWT.php';
  $time = time();
  $key = "usuario";    

class login_model{
  protected $connection;
	function __construct(){
    $db = new dbConnection();
		$this->connection =  $db->db();
  }

  public function index(){
    return $this;//->model;
  }

  public function access(){
    if (isset($_POST['user']) && isset($_POST['password'])) {
      $user = $_POST['user'];
      $password = $_POST['password'];      
      $token_array = array(
        "tiempoInicio"=>$GLOBALS['time'],
        "tiempoFin"=>$GLOBALS['time']+(60*60),
        "user"=> $_POST["user"]
      );

      /* Obtener las categorias */
      $categorias = $this->returnCategorias();
      $modulo = array();
      $arrModulos = array();
      for ($i=0; $i < sizeof( $categorias ); $i++) {
        $modulo[ $categorias[ $i ]['nombre_categoria'] ] = array();
        foreach ($modulo as $key => $value) {
          if ( $key === $categorias[ $i ][ 'nombre_categoria' ] ) {
            $modulo[$key][ $i ] = $categorias[ $i ];
            $arrModulos[ $categorias[ $i ][ 'nombre_categoria' ] ][] = $modulo[$key][ $i ];
          }
        }
      }
      $_SESSION['catModulos'] = $arrModulos;
      
      $usuario = $this->verifyUser( $user , $password );
      // print_r( $usuario );
      $permisosRutas = array();
      $permisos = $this->verifyUserPerm( $usuario[0]['id'] );
      foreach ( $arrModulos as $key => $value ) {
        foreach ( $value as $ke => $val ) {
          foreach ( $permisos as $k => $va ) {
            if ( $va['id_modulo'] === $val['id_modulo'] ) {
              $permisosRutas[] = $va;
            }
          }
        }
      }
      
      $_SESSION['permisosRutas'] = $permisosRutas;

      if (isset($usuario) && $usuario != "null") {
        if ($usuario[0]['estatus'] == 1) {
          $token = JWT::encode($token_array,$GLOBALS['key']);
          setcookie("usuario",$usuario[0]['nombre'],$GLOBALS['time']+(60*60),"/");
          setcookie("token",$token,$GLOBALS['time']+(60*60),"/");
          $_SESSION['tokenUser'] = $token;
          $_SESSION['id_usuario'] = $usuario[0]['id'];
          $_SESSION['id_perfil'] = $usuario[0]['id_perfil'];
          header("Location: ".$_SESSION['linkGlobal']."/index");
        }else {
          $_SESSION['message'] = 'El usuario se encuentra inactivo';
          header("Location: ".$_SESSION['linkGlobal']."/");
        }
      }else{
        $_SESSION['message'] = 'El usuario no ha sido encontrado, compruebe sus credenciales.';
        header("Location: ".$_SESSION['linkGlobal']."/index");
      }
    }
  }

  public function verifyUser($user,$password){
    $sql = "SELECT * FROM fp_usuario WHERE email = BINARY '$user' and contraseña = '".sha1($password)."'";    
    $resultado = $this->connection->query($sql);
		if($resultado){
			$nFilas = $resultado->num_rows;
			if($nFilas > 0){
				while($fila = $resultado->fetch_assoc())
					$dataOut[] = $fila;
				return $dataOut;
			}else
				return "null";
		}else
			return $this->connection->error;
  }
  
  public function verifyUserPerm($id_user){
    $sql = "SELECT * FROM fp_permiso_usuario_modulo T0
    INNER JOIN fp_modulos T1 ON T0.id_modulo = T1.id_modulo
    INNER JOIN fp_acciones T2 ON T0.id_accion = T2.id_accion
    WHERE id_usuario = $id_user";
    $resultado = $this->connection->query($sql);
    if($resultado){
			$nFilas = $resultado->num_rows;
			if($nFilas > 0){
				while($fila = $resultado->fetch_assoc())
					$dataOut[] = $fila;
				return $dataOut;
			}else
				return "null";
		}else
			return $this->connection->error;
  }
  
  public function returnModulos($id_modulo){
    $sql = "SELECT T0.id,T1.* FROM fp_relacion_cat_mod T0
    INNER JOIN fp_modulos T1 ON T0.id_modulo = T1.id_modulo
    WHERE T1.id_modulo = $id_modulo";
    $resultado = $this->connection->query($sql);
    if($resultado){
			$nFilas = $resultado->num_rows;
			if($nFilas > 0){
				while($fila = $resultado->fetch_assoc())
					$dataOut[] = $fila;
				return $dataOut;
			}else
				return "null";
		}else
			return $this->connection->error;
  }
  
  public function returnCategorias(){
    $sql = "SELECT 
      T2.nombre_categoria,
      T1.nombre,
      T1.alias,
      T1.ruta,
      T0.id,
      T0.id_categoria,
      T0.id_modulo
    FROM fp_cat_mod_usu AS T0
    INNER JOIN fp_modulos AS T1 ON T0.id_modulo = T1.id_modulo
    INNER JOIN fp_categorias AS T2 ON T0.id_categoria = T2.id_categoria;";
    
    $resultado = $this->connection->query($sql);
    if($resultado){
			$nFilas = $resultado->num_rows;
			if($nFilas > 0){
				while($fila = $resultado->fetch_assoc())
					$dataOut[] = $fila;
				return $dataOut;
			}else
				return "null";
		}else
			return $this->connection->error;
  }

  public function logout(){
    session_destroy();
    setcookie("usuario","",$GLOBALS['time']-1,"/");
    setcookie("token","",$GLOBALS['time']-1,"/");
    header("Location: ".$_SESSION['linkGlobal']."/index");
  }
  public function __destruct(){
    mysqli_close($this->connection);
  }
}
// print_r( $_SESSION['permisosRutas'] );
// print_r( $_SESSION['catModulos'] );
// echo "<br>";
// echo "<br>";
// echo "<br>";
// echo "<br>";
// $baseDatos = '01';
// //$dsn = 'firebird:dbname=192.168.2.7:C:\\PROGRAM FILES (X86)\\COMMON FILES\\ASPEL\\Sistemas Aspel\\NOI10.00\Datos\\Empresa'.$baseDatos.'\\NOI100EMPRE'.$baseDatos.'.FDB';
// $dsn = 'firebird:dbname=localhost:C:/Program Files (x86)/Common Files/Aspel/Sistemas Aspel/NOI9.00/Datos/Empresa01/NOI90EMPRE'.$baseDatos.'.FDB';
// $username = 'SYSDBA';
// $contraseña = 'masterkey';
// // try 
// // {
//               $conexion = new PDO ($dsn, $username, $contraseña);
//               if($conexion)
//               {
//                             echo "Conexion Establecida"; 
//                             // return $conexion;
//               }
//               else
//                             echo "No fue posible realizar la conexión a Base de Datos Firebird";
// }
// catch( PDOException $Exception ) {
//                             die( $Exception->getMessage( ));
// }
?>