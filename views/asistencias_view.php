<?php
// Verificar login
include(__DIR__.'/verifyLogin.php');

/**
* Dashboard de inicio
*/
  require_once(__DIR__.'/../controllers/horario_controller.php');
  $claseIndex = new horario_controller();
  $res = $claseIndex->asistencias();

?>
<!-- Header -->
<?php include(__DIR__.'/head.php'); ?>
<!-- Header -->
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <!-- row -->
    <div class="container-fluid">				    
        <div class="row">					
            <div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12 col-sm-12">

              <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Listado de Asistencias</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive recentOrderTable">
                        <table class="table verticle-middle table-responsive-md">
                            <thead>
                                <tr>
                                    <th scope="col">Nombre empresa</th>
                                    <th scope="col">Ruta de BD</th>
                                    <th scope="col">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($res as $key => $value) {
                                    echo"
                                    <tr>
                                        <td>".$value['cve_trabajador']."</td>
                                        <td>".$value['fecha']."</td>
                                        <td>".$value['tiempo']."</td>
                                        <td>".$value['tipo_acceso']."</td>                                        
                                    </tr>";
                                } ?>                                                                        
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
        </div>        
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<!-- footer -->
<?php include(__DIR__.'/foot.php');  ?>
<script src="<?php echo $rutaIndexArchivos; ?>/public/js/modulos/index_horario.js"></script>
<!-- footer -->