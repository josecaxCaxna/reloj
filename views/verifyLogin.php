<?php
// Archivo para verificar que el usuario se encuentre correctamente logueado
$host= $_SERVER["HTTP_HOST"];
$url= $_SERVER["REQUEST_URI"];
$exp = explode(  "/", $url);
$exps = $exp[1];
// Condiciones que ambas cookies existan
if ( !isset($_COOKIE['token']) && !isset($_COOKIE['usuario']) && !isset($_SESSION['tokenUser']) ) {
    $_SESSION['message'] = 'Acceso no autorizado';
    header("Location: http://$host/$exps");
}

// Condicion para verificar que la sesion siga activa
if ( !isset($_COOKIE['token']) && !isset($_COOKIE['usuario']) && isset($_SESSION['tokenUser'])) {
    $_SESSION['message'] = 'Su sesión ha caducado.';
    unset($_SESSION['tokenUser']);
    header("Location: http://$host/$exps");
}else if ($_SESSION['tokenUser'] != $_COOKIE['token']) { // Condicion para que el Token en el servidor coincida con el token del navegador(cookie)
    $_SESSION['message'] = 'Token de usuario incorrecto.';
    header("Location: http://$host/$exps");
}
?>