<?php
// Verificar login
include(__DIR__.'/verifyLogin.php');

/**
* Dashboard de inicio
*/
  require_once(__DIR__.'/../controllers/index_controller.php');
  $claseIndex = new index_controller();
  $res = $claseIndex->cargarEmpresas();
//   print_r($_SESSION['datosUsuario']);
?>
<!-- Header -->
<?php include(__DIR__.'/head.php'); ?>
<!-- Header -->
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <!-- row -->
    <div class="container-fluid">				    
        <div class="row">					
            <div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Empresas / Sucursales</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive recentOrderTable">
                            <table class="table verticle-middle table-responsive-md">
                                <thead>
                                    <tr>
                                        <th scope="col">Nombre empresa</th>
                                        <th scope="col">Ruta de BD</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($res as $key => $value) {
                                        echo"
                                        <tr>
                                            <td>".$value['nombre']."</td>
                                            <td>".$value['directorio_db']."</td>
                                            <td>
                                                <a href='".$value['id']."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a>
                                                <a href='".$value['id']."' class='btn btn-sm btn-danger'><i class='la la-trash-o'></i></a>
                                            </td>
                                        </tr>";
                                    } ?>                                                                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<!-- footer -->
<?php include(__DIR__.'/foot.php');  ?>
<!-- footer -->