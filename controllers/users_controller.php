<?php
    require_once(__DIR__.'/../models/users_model.php');
    /**
    * The home page controller
    */
    class users_controller
    {
        function __construct()
        {
            $this->model = new users_model();
        }        
        
        public function listarUsuarios()
        {
            return $this->model->listarUsuarios();
        }
        
        public function editarUsuario()
        {
            echo json_encode($this->model->editarUsuario());
        }

    }
?>