<?php
// Verificar login
include(__DIR__.'/verifyLogin.php');

/**
* Dashboard de inicio
*/
  require_once(__DIR__.'/../controllers/horario_controller.php');
  $claseIndex = new horario_controller();
  $res = $claseIndex->asistencias();
//   print_r($_SESSION['datosUsuario']);
            // $booleanGrupo = false;
            $booleanGrupo = true;
            $booleanHorario = true;
            $booleanAsignacionHorario= true;
            $booleanRepAsistencias = true;
            $booleanHrsExtras = true;
            $booleanListaAsistencias = true;
            $booleanPrimaDominical = true;
            $booleanPremioPuntualidad = true;
            $booleanRegistroFaltas = true;

            // $accesoCtrlHorario = json_decode($permisos['ctrl_horario']);
            // IF(!is_null($accesoCtrlHorario))
            //     for($i = 0; $i < count($accesoCtrlHorario->ctrlHorario); $i++)
            //     {
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'grupo') /// Mod 1
            //             $booleanGrupo = true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'horario') /// Mod 2
            //             $booleanHorario = true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'asignacionHorario')  /// Mod 3
            //             $booleanAsignacionHorario= true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'reporteAsistencias')  /// Mod 4
            //             $booleanRepAsistencias = true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'hrsExtras')  /// Mod 5
            //             $booleanHrsExtras = true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'listaAsistencias')  /// Mod 6
            //             $booleanListaAsistencias = true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'primaDominical')  /// Mod 7
            //             $booleanPrimaDominical = true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'premioPuntualidad')  /// Mod 8
            //             $booleanPremioPuntualidad = true;
            //         if($accesoCtrlHorario->ctrlHorario[$i] === 'registroFaltas')  /// Mod 8
            //             $booleanRegistroFaltas = true;
            //     }

            $classGrupo = '';
            $classHorario = '';
            $classDepartamento = '';
            $classRepAsistencias = '';
            $classHrsExtras = '';
            $classListaAsistencias = '';
            $classPrimaDominical = '';
            $classPremioPuntualidad = '';
            $classRegistroFaltas = '';

            if($booleanGrupo)
                $classGrupo = 'active';
            elseif($booleanHorario)
                $classHorario = 'active';
            elseif($booleanAsignacionHorario)
                $classDepartamento = 'active';
            elseif($booleanRepAsistencias)
                $classRepAsistencias = 'active';
            elseif($booleanHrsExtras)
                $classHrsExtras = 'active';
            elseif($booleanListaAsistencias)
                $classListaAsistencias = 'active';
            elseif($booleanPrimaDominical)
                $classPrimaDominical = 'active';
            elseif($booleanPremioPuntualidad)
                $classPremioPuntualidad = 'active';
            elseif($booleanRegistroFaltas)
                $classRegistroFaltas = 'active';


            ///Emails y contraseñas para el envio de email en el Mod de horas extras
            
            $email = array(
                'rh@indelpa.com.mx',
                'rsanabria@indelpa.com.mx',
                'gonofre@indelpa.com.mx', 
                'ejimenez@indelpa.com.mx', 
                'daraujo@indelpa.com.mx',
                'hcaballero@indelpa.com.mx',
                'amartinez@indelpa.com.mx',
                'alexreyes@indelpa.com.mx'
            );

            $passwords = array(
                'Rh1nd3lp4',
                'R4f41nd3lp4',
                'gal2323', 
                'Er1k41nd3lp4', 
                'D4v1d1nd3lp4',
                'H3ct0r1nd3lp4',
                'August01nd3lp4',
                'Al3x1nd3lp4'
            );

?>
<!-- Header -->
<?php include(__DIR__.'/head.php'); ?>
<!-- Header -->
<!--**********************************
    Content body start
***********************************-->
<div class="content-body">
    <!-- row -->
    <div class="container-fluid">				    
        <div class="row">					
            <div class="col-xl-12 col-xxl-12 col-lg-12 col-md-12 col-sm-12">

            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Listado de Asistencias</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive recentOrderTable">
                        <table class="table verticle-middle table-responsive-md">
                            <thead>
                                <tr>
                                    <th scope="col">No. Empleado</th>
                                    <th scope="col">Fecha</th>
                                    <th scope="col">Hora</th>
                                    <th scope="col">Tipo </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($res as $key => $value) {
                                    echo"
                                    <tr>
                                        <td>".$value['cve_trabajador']."</td>
                                        <td>".$value['fecha']."</td>
                                        <td>".$value['tiempo']."</td>
                                        <td>".$value['tipo_acceso']."</td>                                        
                                    </tr>";
                                } ?>                                                                        
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>



            <!-- <section class="content-header">
          <h1>&nbsp;</h1>
          <ol class="breadcrumb">
            <li><a href="../../indsoft"><i class="fa fa-home"></i> Inicio</a></li>
            <li class="active">Horarios</li>
          </ol>
        </section>
        <section class="content">
          <div class="box box-solid box-primary box-shadow animated zoomIn">
            <div class="box-header">
              <h3 class="box-title"><strong>Horarios</strong></h3>
            </div>
            <div class="box-body">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs card-body-tabs">

                  <?php if($booleanRegistroFaltas):?>
                  <li <?php echo "class='".$classRegistroFaltas."'";?>><a href="#mod-faltas" data-toggle="tab">Regitro de Faltas</a></li>
                  <?php endif; ?>

                  <?php if($booleanPremioPuntualidad):?>
                  <li <?php echo "class='".$classPremioPuntualidad."'";?>><a href="#mod-premioPuntualidad" data-toggle="tab">Premio de Puntualidad</a></li>
                  <?php endif; ?>

                  <?php if($booleanPrimaDominical):?>
                  <li <?php echo "class='".$classPrimaDominical."'";?>><a href="#mod-primaDominical" data-toggle="tab">Prima Dominical</a></li>
                  <?php endif; ?>

                  <?php if($booleanListaAsistencias):?>
                  <li <?php echo "class='".$classRepAsistencias."'";?>><a href="#listado_asistencias" data-toggle="tab">RH Lista de asistencias</a></li>
                  <?php endif; ?>

                  <?php if($booleanHrsExtras):?>
                  <li <?php echo "class='".$classHrsExtras."'";?>><a href="#horasExtras" data-toggle="tab">Horas Extras</a></li>
                  <?php endif; ?>

                  <?php if($booleanRepAsistencias):?>
                  <li <?php echo "class='".$classRepAsistencias."'";?>><a href="#formato" data-toggle="tab">Reporte de asistencias</a></li>
                  <?php endif; ?>

                  <?php if($booleanAsignacionHorario):?>
                  <li <?php echo "class='".$classDepartamento."'";?>><a href="#asignacion-horario" data-toggle="tab">Asignación de Horario</a></li>
                  <?php endif; ?>

                  <?php if($booleanHorario):?>
                  <li <?php echo "class='".$classHorario."'";?>><a href="#registro-horario" data-toggle="tab">Horario</a></li>
                  <?php endif; ?>

                  <?php if($booleanGrupo):?>
                  <li <?php echo "class='".$classGrupo."'";?>><a href="#registro-grupos" data-toggle="tab">Grupo</a></li>
                  <?php endif; ?>

                </ul>
                <div class="tab-content no-padding">

                  <?php if($booleanRegistroFaltas):?>
                  <div class="tab-pane  <?php echo $classRegistroFaltas;?>" id="mod-faltas" style="position: relative; height: auto;">
                    <div class="col-md-12">
                      <hr>
                      <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="empresaRF" class="control-label">Empresa:</label>
                            <select name="empresaRF" id="empresaRF" class="selectpicker form-control input-sm" data-style="btn-info" required>
                              
                              <?php if(!empty($permisos['toluca_departamento'])){ ?>
                              <option class="text-center" selected>TOLUCA</option>
                              <?php } ?>
                             
                              <?php if(!empty($permisos['bosques_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >BOSQUES</option>
                              <?php } ?>
                              
                              <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >COHISA</option>
                              <?php } ?>

                            </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label for="fechaNominaRF" class="control-label">Fecha de Nómina:</label>
                          <select name="fechaNominaRF" id="fechaNominaRF" class="selectpicker form-control input-sm" data-style="btn-info" title="--- Seleccionar Nómina ---" required>
                            <?php
                              foreach ($fechaNominaAGH as $key => $datos) {

                                $keyArray = array_search($datos["fecha_nomina"],array_column($fullFechaNomina, 'FECHNOMI'));
                                $fecha = strtotime($datos["fecha_nomina"]);
                                echo "<option value='".$datos["fecha_nomina"]."'>Nom: ".$fullFechaNomina[$keyArray]['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="row" id='loadTblRf'>
                        <div class="col-md-12 table-responsive">
                          <table id="tblPersonalRF" class="table table-hover table-striped table-bordered">
                            <thead>
                              <tr>
                                <th>Clave Trabajador</th>
                                <th>Nombre</th>
                                <th>Departamento</th>
                                <th>Día Falta</th>
                                <th>Fecha inicial</th>
                                <th>Fecha Final</th>
                                <th>Tipo Falta</th>
                                <th>Certificado IMSS</th>
                                <th>Tipo Incapacidad</th>
                                <th>% Falta</th>
                                <th>Duplicar</th>
                              </tr>
                            </thead>
                            <tbody class="jumbotron">
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="pull-right">
                            <button id="btnExportarRF" class="btn btn-sm btn-success" disabled>
                              <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
                              Exportar
                            </button>
                            <button id="btnGuardarRF" class="btn btn-sm btn-primary" disabled>
                              <span class="fa fa-save" aria-hidden="true"></span>
                              Guardar
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>

                  <?php if($booleanPremioPuntualidad):?>
                  <div class="tab-pane  <?php echo $classPremioPuntualidad;?>" id="mod-premioPuntualidad" style="position: relative; height: auto;">
                    <div class="col-md-12">
                      <hr>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-shadow">
                          <div class="box-header">
                            <h3 class="box-title">Registro de Premio de Puntualidad</h3>
                          </div>
                          <div class="box-body">
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="empresaPP" class="control-label">Empresa:</label>
                                  <select name="empresaPP" id="empresaPP" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                    
                                    <?php if(!empty($permisos['toluca_departamento'])){ ?>
                                    <option class="text-center" selected>TOLUCA</option>
                                    <?php } ?>
                                   
                                    <?php if(!empty($permisos['bosques_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >BOSQUES</option>
                                    <?php } ?>
                                    
                                    <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >COHISA</option>
                                    <?php } ?>

                                  </select>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <label for="fechaNominaPP" class="control-label">Fecha de Nómina:</label>
                                <select name="fechaNominaPP" id="fechaNominaPP" class="selectpicker form-control input-sm" data-style="btn-info" title="--- Seleccionar Nómina ---" required>
                                  <?php
                                    foreach ($fechaNominaAGH as $key => $datos) {
                                      # code...
                                      $keyArray = array_search($datos["fecha_nomina"],array_column($fullFechaNomina, 'FECHNOMI'));
                                      $fecha = strtotime($datos["fecha_nomina"]);
                                      echo "<option value='".$datos["fecha_nomina"]."'>Nom: ".$fullFechaNomina[$keyArray]['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                    }
                                  ?>
                                </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12 table-responsive">
                                <table id="tblPersonalPP" class="table table-hover table-striped table-bordered nowrap">
                                  <thead>
                                    <tr>
                                      <th class="col-head" colspan="3"></th>
                                      <th></th>
                                      <th colspan="2">Lunes</th>
                                      <th colspan="2">Martes</th>
                                      <th colspan="2">Miercoles</th>
                                      <th colspan="2">Jueves</th>
                                      <th colspan="2">Viernes</th>
                                      <th colspan="2">Sabado</th>
                                      <th colspan="2">Domingo</th>
                                    </tr>
                                    <tr>
                                      <th class="col00">PP</th>
                                      <th class="col11">PP-A</th>
                                      <th class="col22">Clave Trabajador</th>
                                      <th>Nombre Trabajador</th>
                                      <th>Hora de Entrada</th>
                                      <th>Hora Registrada</th>
                                      <th>Hora de Entrada</th>
                                      <th>Hora Registrada</th>
                                      <th>Hora de Entrada</th>
                                      <th>Hora Registrada</th>
                                      <th>Hora de Entrada</th>
                                      <th>Hora Registrada</th>
                                      <th>Hora de Entrada</th>
                                      <th>Hora Registrada</th>
                                      <th>Hora de Entrada</th>
                                      <th>Hora Registrada</th>
                                      <th>Hora de Entrada</th>
                                      <th>Hora Registrada</th>
                                    </tr>
                                  </thead>
                                  <tbody class="jumbotron">
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <div class="box-footer">
                            <div class="pull-right">

                              <?php if(isset($arrayPerfil[28][3]) && $arrayPerfil[28][3] === "Exportar"){ ?>
                                <button id="btnExportarPP" class="btn btn-sm btn-success" style="display: none;">
                                  <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
                                  Exportar reporte
                                </button>
                              <?php } ?>

                              <button class="btn btn-sm btn-primary" id="btnGuardarRegPP" disabled>
                                <span class="fa fa-save" aria-hidden="true"></span>
                                Guardar
                              </button>
                            </div>
                          </div>
                        </div>
                        </div>
                        <div class="col-md-6"></div>
                      </div>
                    </div>
                   </div>
                   <?php endif; ?>

                   <?php if($booleanPrimaDominical):?>
                   <div class="tab-pane  <?php echo $classPrimaDominical;?>" id="mod-primaDominical" style="position: relative; height: auto;">
                    <div class="col-md-12">
                      <hr>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="box box-shadow">
                          <div class="box-header">
                            <h3 class="box-title">Registro de Prima Dominical</h3>
                          </div>
                          <div class="box-body">
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="empresaPD" class="control-label">Empresa:</label>
                                  <select name="empresaPD" id="empresaPD" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                    
                                    <?php if(!empty($permisos['toluca_departamento'])){ ?>
                                    <option class="text-center" selected>TOLUCA</option>
                                    <?php } ?>
                                   
                                    <?php if(!empty($permisos['bosques_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >BOSQUES</option>
                                    <?php } ?>
                                    
                                    <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >COHISA</option>
                                    <?php } ?>

                                  </select>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <label for="fechaNominaPD" class="control-label">Fecha de Nómina:</label>
                                <select name="fechaNominaPD" id="fechaNominaPD" class="selectpicker form-control input-sm" data-style="btn-info" title="--- Seleccionar Nómina ---" required>
                                  <?php
                                    foreach ($fullFechaNomina as $key => $datos) {
                                      $mes = date('m');
                                      $anio = strtotime(date('Y'));
                                      $anioAnterior = strtotime('-1 year' , $anio);
                                      $anioAnterior = $mes > 01 ?   $anio : $anioAnterior;
                                      $fecha = strtotime($datos["FECHNOMI"]);

                                      if(strtotime(date('Y', $fecha)) >= strtotime(date('Y', $anioAnterior)) AND strtotime(date('Y', $fecha)) <= $anio)
                                      {
                                          $fecha = strtotime($datos["FECHNOMI"]);
                                          echo "<option value='".$datos["FECHNOMI"]."'>Nom: ".$datos['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                      }
                                    }
                                  ?>
                                </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12 table-responsive">
                                <table id="tblPersonalPD" class="table table-hover table-striped table-bordered nowrap">
                                  <thead>
                                    <tr>
                                      <th>Prima Dominical</th>
                                      <th>Ayuda de Transporte</th>
                                      <th>Departamento</th>
                                      <th>Clave Trabajador</th>
                                      <th>Nombre Trabajador</th>
                                    </tr>
                                  </thead>
                                  <tbody class="jumbotron">
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                          <div class="box-footer">
                            <div class="pull-right">
                              <?php if(isset($arrayPerfil[28][3]) && $arrayPerfil[28][3] === "Exportar"){ ?>
                                <button id="btnExportarPD" class="btn btn-sm btn-success" style="display: none;">
                                  <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
                                  Exportar reporte
                                </button>
                              <?php } ?>

                              <button class="btn btn-sm btn-primary" id="btnGuardarRegPD" disabled>
                                <span class="fa fa-save" aria-hidden="true"></span>
                                Guardar
                              </button>

                            </div>
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                   </div>
                  <?php endif; ?>

                  <?php if($booleanHrsExtras):?>
                  <div class="tab-pane  <?php echo $classHrsExtras;?>" id="horasExtras" style="position: relative; height: auto;">
                    
                    <div class="col-md-12">
                      <hr>
                      <div class="row">
                        <div class="col-md-1" style="font-weight: bold; font-size: 15px">
                          <div class="bg-info">A : ASISTENCIA</div>
                          <div class="bg-red">F : FALTA</div>
                        </div>
                        <div class="col-md-2" style="font-weight: bold; font-size: 15px">
                          <div class="bg-info">D : DESCANSO</div>
                          <div class="bg-danger">VAC : VACACIONES</div>
                        </div>
                        <div class="col-md-2" style="font-weight: bold; font-size: 15px">
                          <div class="bg-yellow">INC : INCAPACIDAD</div>
                          <div class="bg-danger">B : BAJA</div>
                        </div>
                        <div class="col-md-2" style="font-weight: bold; font-size: 15px">
                          <div class="bg-info">NI : NVO INGRESO</div>
                          <div class="bg-danger">S : SANCIÓN (DÍA SIN GOCE DE SUELDO)</div>
                         </div>
                        <div class="col-md-2" style="font-weight: bold; font-size: 15px">
                          <div class="bg-blue">P : PERMISO</div>
                          <div class="bg-danger">DF : DIA FESTIVO</div>
                        </div>
                        <div class="col-md-3" style="font-weight: bold; font-size: 15px">
                          <div class="bg-orange">FJ : FALTA JUSTIFICADA</div>
                          <div class="bg-info">N/A : No Aplica - Aún no se había contratado.</div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="control-label">Empresa:</label>
                            <select name="empresa" id="empresaHorasExtras" class="selectpicker form-control input-sm" data-style="btn-info" required>
                              
                              <?php if(!empty($permisos['toluca_departamento'])){ ?>
                              <option class="text-center" selected>TOLUCA</option>
                              <?php } ?>
                             
                              <?php if(!empty($permisos['bosques_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >BOSQUES</option>
                              <?php } ?>
                              
                              <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >COHISA</option>
                              <?php } ?>

                            </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label for="departamentoHorasExtras" class="control-label">Departamento:</label>
                            <select name="departamentoHorasExtras" id="departamentoHorasExtras" class="selectpicker form-control input-sm" title="--- Seleccionar Departamento ---" data-style="btn-info">
                              <?php
                              foreach ($departamento as $key => $value) {
                                # code...
                                echo '<option value="'.trim($value["CLAVE"]).'">'.utf8_encode($value["NOMBRE"]).'</option>';
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label for="fInicialHorasExtras" class="control-label">Fecha de nómina:</label>
                          <div class="form-group has-feedback">
                            <select name="fechaNominaHorasExtras" id="fechaNominaHorasExtras" class="selectpicker form-control input-sm" data-style="btn-info" required>
                              <?php
                                foreach ($fechaNominaAGH as $key => $datos) {
                                  # code...
                                  $keyArray = array_search($datos["fecha_nomina"],array_column($fullFechaNomina, 'FECHNOMI'));
                                  $fecha = strtotime($datos["fecha_nomina"]);
                                  echo "<option value='".$datos["fecha_nomina"]."'>Nom: ".$fullFechaNomina[$keyArray]['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                }
                              ?>
                            </select>
                          </div>
                        </div>

                        <div class="col-md-3">
                        <div class="form-group">
                          <label for="grupoAsignadoUsu" class="control-label">Grupo:</label>
                          <select name="grupoAsignadoUsu" id="grupoAsignadoUsu" class="selectpicker form-control input-sm" data-selected-text-format="count > 4" data-actions-box="true" multiple title="--- Seleccionar Grupo ---" data-style="btn-info">                          
                          </select>
                        </div>
                        </div>

                        <div class="col-md-1" align="center">
                          <label>&nbsp;</label>
                          <div class="input-group input-group-sm">
                            <button name="btnBuscar" id="btnBuscarHorasExtras" class="btn btn-primary btn-sm">
                              <span class="fa fa-search" aria-hidden="true"></span>
                              Buscar
                            </button>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-12 table-responsive">
                          <table id="tblHorasExtras" class="table table-hover table-striped table-bordered nowrap">
                            <thead>
                              <tr>
                                <th class="col0" colspan="2"></th>                                
                                <th colspan="3">Lunes</th>
                                <th colspan="3">Martes</th>
                                <th colspan="3">Miercoles</th>
                                <th colspan="3">Jueves</th>
                                <th colspan="3">Viernes</th>
                                <th colspan="3">Sabado</th>
                                <th colspan="3">Domingo</th>
                                <th></th>
                              </tr>
                              <tr>
                                <th class="col1"># Trabajador</th>
                                <th class="col2">Nombre del trabajador</th>
                                <th>ASIST</th>
                                <th>T. E.</th>
                                <th>T. A.</th>
                                <th>ASIST</th>
                                <th>T. E.</th>
                                <th>T. A.</th>
                                <th>ASIST</th>
                                <th>T. E.</th>
                                <th>T. A.</th>
                                <th>ASIST</th>
                                <th>T. E.</th>
                                <th>T. A.</th>
                                <th>ASIST</th>
                                <th>T. E.</th>
                                <th>T. A.</th>
                                <th>ASIST</th>
                                <th>T. E.</th>
                                <th>T. A.</th>
                                <th>ASIST</th>
                                <th>T. E.</th>
                                <th>T. A.</th>
                                <th>Hrs. Totales</th>
                              </tr>
                            </thead>
                            <tbody class="jumbotron">
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="pull-right">
                            
                            <button id="btnReporteHrsExtras" class="btn btn-sm btn-danger" style="display: none;">
                              <span class="fa fa-file-pdf-o"></span>
                              PDF reporte
                            </button>
                            <?php if(isset($arrayPerfil[28][3]) && $arrayPerfil[28][3] === "Exportar"){ ?>
                            <button id="btnExportarHrsExtrasRH" class="btn btn-sm btn-success" style="display: none;">
                              <span class="glyphicon glyphicon-export"></span>
                              Exportar Horas Extras
                            </button>

                            <button id="btnExportarHrsExtras" class="btn btn-sm btn-success" style="display: none;">
                              <span class="glyphicon glyphicon-export" aria-hidden="true"></span>
                              Exportar reporte
                            </button>
                            <?php } ?>

                            <?php if(isset($arrayPerfil[28][6]) && $arrayPerfil[28][6] === "Revisado"){ ?>
                            <button id="btnRevisado" class="btn btn-sm btn-info" style="display: none;">
                              <span class="fa fa-eye" aria-hidden="true"></span>
                              Revisado
                            </button>
                            <?php } ?>

                            <?php if(isset($arrayPerfil[28][7]) && $arrayPerfil[28][7] === "Autorizar"){ ?>
                            <button id="btnAutorizar" class="btn btn-sm btn-warning" style="display: none;">
                              <span class="fa fa-check" aria-hidden="true"></span>
                              Autorizar
                            </button>
                            <?php } ?>

                            <button id="btnGuardarHrsExtras" class="btn btn-sm btn-primary" disabled>
                              <span class="fa fa-save" aria-hidden="true"></span>
                              Guardar
                            </button>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>

                  <?php if($booleanListaAsistencias):?>
                  <div class="tab-pane  <?php echo $classRepAsistencias;?>" id="listado_asistencias" style="position: relative; height: auto;">
                    <div class="col-md-12">
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="control-label">Empresa:</label>
                            <select name="empresa" id="empresaListaAsistencia" class="selectpicker form-control input-sm" data-style="btn-info" required>
                              
                              <?php if(!empty($permisos['toluca_departamento'])){ ?>
                              <option class="text-center" selected>TOLUCA</option>
                              <?php } ?>
                             
                              <?php if(!empty($permisos['bosques_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >BOSQUES</option>
                              <?php } ?>
                              
                              <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >COHISA</option>
                              <?php } ?>

                            </select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="control-label">Grupos:</label>
                            <select name="departamento" id="grupoListaAsistencia" class="selectpicker form-control input-sm" title="--- Seleccionar Grupo ---" data-style="btn-info">
                              <?php
                              foreach ($grupos as $key => $value) {
                                if($value["TEXTO01"] != '')
                                  echo '<option value="'.trim(utf8_encode($value["TEXTO01"])).'">'.utf8_encode($value["TEXTO01"]).'</option>';
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label for="fechaNominaAsistencias" class="control-label">Fecha de Nómina:</label>
                          <select name="fechaNominaAsistencias" id="fechaNominaListaAsistencia" class="selectpicker form-control input-sm" data-style="btn-info" required>
                            <?php
                              foreach ($fullFechaNomina as $key => $datos) {
                                # code...
                                $mes = date('m');
                                $anio = strtotime(date('Y'));
                                $anioAnterior = strtotime('-1 year' , $anio);
                                $anioAnterior = $mes > 01 ?   $anio : $anioAnterior;
                                $fecha = strtotime($datos["FECHNOMI"]);

                                if(strtotime(date('Y', $fecha)) >= strtotime(date('Y', $anioAnterior)) AND strtotime(date('Y', $fecha)) <= $anio)
                                {

                                  $fecha = strtotime($datos["FECHNOMI"]);
                                  echo "<option value='".$datos["FECHNOMI"]."'>Nom: ".$datos['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                }
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-md-12 col-lg-1" align="center">
                          <label>&nbsp;</label>
                          <div class="input-group input-group-sm">
                            <button name="btnBuscar" id="btnBuscarListaAsistencia" class="btn btn-primary btn-sm">
                              <span class="fa fa-search" aria-hidden="true"></span>
                              Buscar
                            </button>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="pull-right">
                            <button class="btn btn-sm btn-danger" id="btnReporteListaAsistencia"><span class="fa fa-file-pdf-o"></span> Generar Reporte</button>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-md-12 table-responsive">
                          <table id="tblListaAsistencia" class="table table-hover table-striped table-bordered nowrap">
                            <thead>
                              <th>Departamento</th>
                              <th>Clave Trabajador</th>
                              <th>Nombre Trabajador</th>
                              <th>Fecha</th>
                              <th>Tiempo</th>
                              <th>Entrada / Salida</th>
                            </thead>
                            <tbody class="jumbotron">
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>

                  <?php if($booleanRepAsistencias):?>
                  <div class="tab-pane  <?php echo $classRepAsistencias;?>" id="formato" style="position: relative; height: auto;">
                    <div class="col-md-12">
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="control-label">Empresa:</label>
                            <select name="empresa" id="empresaReporte" class="selectpicker form-control input-sm" data-style="btn-info" required>
                              
                              <?php if(!empty($permisos['toluca_departamento'])){ ?>
                              <option class="text-center" selected>TOLUCA</option>
                              <?php } ?>
                             
                              <?php if(!empty($permisos['bosques_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >BOSQUES</option>
                              <?php } ?>
                              
                              <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >COHISA</option>
                              <?php } ?>

                            </select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="control-label">Departamento:</label>
                            <select name="departamento" id="departamentoReporte" class="selectpicker form-control input-sm" title="--- Seleccionar Departamento ---" data-style="btn-info">
                              <?php
                              foreach ($departamento as $key => $value) {
                                # code...
                                echo '<option value="'.trim($value["CLAVE"]).'">'.utf8_encode($value["NOMBRE"]).'</option>';
                              }
                              ?>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <label for="fechaNominaAsistencias" class="control-label">Fecha de Nómina:</label>
                          <select name="fechaNominaAsistencias" id="fechaNominaAsistencias" class="selectpicker form-control input-sm" data-style="btn-info" required>
                            <?php
                              foreach ($fullFechaNomina as $key => $datos) {
                                # code...
                                $mes = date('m');
                                $anio = strtotime(date('Y'));
                                $anioAnterior = strtotime('-1 year' , $anio);
                                $anioAnterior = $mes > 01 ?   $anio : $anioAnterior;
                                $fecha = strtotime($datos["FECHNOMI"]);

                                if(strtotime(date('Y', $fecha)) >= strtotime(date('Y', $anioAnterior)) AND strtotime(date('Y', $fecha)) <= $anio)
                                {
                                  $fecha = date("d-m-Y",$fecha);
                                  echo "<option value='".$datos["FECHNOMI"]."'>Nom: ".$datos['NUM_NOMINA']." ".$fecha."</option>";
                                }
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-md-12 col-lg-1" align="center">
                          <label>&nbsp;</label>
                          <div class="input-group input-group-sm">
                            <button name="btnBuscar" id="btnBuscar" class="btn btn-primary btn-sm">
                              <span class="fa fa-search" aria-hidden="true"></span>
                              Buscar
                            </button>
                          </div>
                        </div>
                      </div>
                      <hr>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="pull-right">
                            <button class="btn btn-sm btn-danger" id="btnReporte"><span class="fa fa-file-pdf-o"></span> Generar Reporte</button>
                          </div>
                        </div>
                      </div>
                      <br>
                      <div class="row" id="loadTblAsistencias">
                        <div class="col-md-12 table-responsive">
                          <table id="tblAsistencias" class="table table-hover table-striped table-bordered nowrap">
                            <thead>
                              <th>#</th>
                              <th>Departamento</th>
                              <TH>Grupo asignado</TH>
                              <th>Clave Trabajador</th>
                              <th>Nombre Trabajador</th>
                              <th>Fecha</th>
                              <th>Tiempo</th>
                              <th>Entrada / Salida</th>
                            </thead>
                            <tbody class="jumbotron">
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>


                  <?php if($booleanAsignacionHorario):?>
                  <div class="tab-pane <?php echo $classDepartamento;?>" id="asignacion-horario" style="position: relative; height: auto;">
                    <div class="col-md-12">
                      <hr>
                      <div class="row">
                        <div class="col-md-2">
                          <div class="form-group">
                            <label class="control-label">Empresa:</label>
                            <select name="empresa" id="empresa" class="selectpicker form-control input-sm" data-style="btn-info" required>
                              
                              <?php if(!empty($permisos['toluca_departamento'])){ ?>
                              <option class="text-center" selected>TOLUCA</option>
                              <?php } ?>
                             
                              <?php if(!empty($permisos['bosques_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >BOSQUES</option>
                              <?php } ?>
                              
                              <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                              <option data-divider="true"></option>
                              <option class="text-center" >COHISA</option>
                              <?php } ?>

                            </select>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <label for="fechaNomina" class="control-label">Fecha de Nómina:</label>
                          <select name="fechaNomina" id="fechaNomina" class="selectpicker form-control input-sm" data-style="btn-info" required>
                            <?php
                              foreach ($fechaNominaGE as $key => $datos) {
                                # code...
                                $keyArray = array_search($datos["fecha_nomina"],array_column($fullFechaNomina, 'FECHNOMI'));
                                $fecha = strtotime($datos["fecha_nomina"]);
                                echo "<option value='".$datos["fecha_nomina"]."'>Nom: ".$fullFechaNomina[$keyArray]['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                              }
                            ?>
                          </select>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <label class="control-label">Ubicacion:</label>
                            <select name="ubicacion" id="ubicacion" class="form-control input-sm" title="--- Seleccionar Ubicación ---" data-style="btn-info">
                              <option data-divider="true"></option>
                              <option>Definir nuevo</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12 table-responsive">
                          <table id="tblGrupoHorario" class="table table-hover table-striped table-bordered nowrap">
                            <thead>
                              <th>#</th>
                              <th>Grupo</th>
                              <th>Horario</th>
                              <th>Ubicación</th>
                            </thead>
                            <tbody class="jumbotron">
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-12">
                          <div class="pull-right">
                            <button id="btnGuardar" class="btn btn-sm btn-primary" disabled>
                              <span class="fa fa-save" aria-hidden="true"></span>
                              Guardar
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>

                  <?php if($booleanHorario):?>
                  <div class="tab-pane <?php echo $classHorario;?>" id="registro-horario" style="position: relative; height: auto;">
                    <hr>
                    <div class="col-md-6">
                      <div class="box box-shadow">
                        <div class="box-header">
                          <h3 class="box-title">Alta de horario</h3>
                        </div>
                        <div class="box-body">
                          <div class="col-md-12">
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label class="control-label">Empresa:</label>
                                  <select name="empresaHorario" id="empresaHorario" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                    
                                    <?php if(!empty($permisos['toluca_departamento'])){ ?>
                                    <option class="text-center" selected>TOLUCA</option>
                                    <?php } ?>
                                   
                                    <?php if(!empty($permisos['bosques_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >BOSQUES</option>
                                    <?php } ?>
                                    
                                    <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >COHISA</option>
                                    <?php } ?>

                                  </select>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <label for="fechaNominaHorario" class="control-label">Fecha de Nómina:</label>
                                <select name="fechaNominaHorario" id="fechaNominaHorario" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                  <?php
                                    foreach ($fechaNomina as $key => $datos) {
                                      # code...
                                      $fecha = strtotime($datos["FECHNOMI"]);
                                      echo "<option value='".$datos["FECHNOMI"]."'>Nom: ".$datos['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                    }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">Nombre Horario:</label>
                                  <input type="text" id="nombreHorario" class="form-control input-sm" placeholder="Nombre de horario">
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12 table-responsive">
                                <table id="tblAltaHorario" class="table table-hover table-striped table-bordered nowrap">
                                  <thead>
                                    <th></th>
                                    <th>Día</th>
                                    <th>Hora Inicial</th>
                                    <th>Hora Final</th>
                                  </thead>
                                  <tbody class="jumbotron">
                                    <tr>
                                      <td class="text-center">
                                        <input class="form-check-input checkDia checkLun" type="checkbox">
                                      </td>
                                      <td>Lunes</td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaInicial" placeholder="Horario inicial">
                                      </td>
                                      <td>
                                          <input type="text" class="form-control input-sm text-center timepicker horaFinal" placeholder="Horario final">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="text-center">
                                        <input class="form-check-input checkDia checkMar" type="checkbox">
                                      </td>
                                      <td>Martes</td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaInicial" placeholder="Horario inicial">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaFinal" placeholder="Horario final">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="text-center">
                                        <input class="form-check-input checkDia checkMie" type="checkbox">
                                      </td>
                                      <td>Miercoles</td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaInicial" placeholder="Horario inicial">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaFinal" placeholder="Horario final">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="text-center">
                                        <input class="form-check-input checkDia checkJue" type="checkbox">
                                      </td>
                                      <td>Jueves</td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaInicial" placeholder="Horario inicial">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaFinal" placeholder="Horario final">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="text-center">
                                        <input class="form-check-input checkDia checkVie" type="checkbox">
                                      </td>
                                      <td>Viernes</td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaInicial" placeholder="Horario inicial">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaFinal" placeholder="Horario final">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="text-center">
                                        <input class="form-check-input checkDia checkSab" type="checkbox">
                                      </td>
                                      <td>Sábado</td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaInicial" placeholder="Horario inicial">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaFinal" placeholder="Horario final" >
                                      </td>
                                    </tr>
                                    <tr>
                                      <td class="text-center">
                                        <input class="form-check-input checkDia checkDom" type="checkbox">
                                      </td>
                                      <td>Domingo</td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaInicial" placeholder="Horario inicial">
                                      </td>
                                      <td>
                                        <input type="text" class="form-control input-sm text-center timepicker horaFinal" placeholder="Horario final">
                                      </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="pull-right">
                            <button class="btn btn-sm btn-primary" id="btnCrearHorario" disabled>
                              Crear
                            </button>
                            <button class="btn btn-default btn-sm" id="btnCancelar">
                              Cancelar
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="box box-shadow">
                        <div class="box-header">
                          <h3 class="box-title">Horarios establecidos</h3>
                        </div>
                        <div class="box-body">
                          <div class="col-md-12">
                            <div class="row">
                              <div class="col-md-3">
                                <div class="form-group">
                                  <label for="empresaHorarioEstablecido" class="control-label">Empresa:</label>
                                  <select name="empresaHorarioEstablecido" id="empresaHorarioEstablecido" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                    
                                    <?php if(!empty($permisos['toluca_departamento'])){ ?>
                                    <option class="text-center" selected>TOLUCA</option>
                                    <?php } ?>
                                   
                                    <?php if(!empty($permisos['bosques_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >BOSQUES</option>
                                    <?php } ?>
                                    
                                    <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                                    <option data-divider="true"></option>
                                    <option class="text-center" >COHISA</option>
                                    <?php } ?>

                                  </select>
                                </div>
                              </div>
                              <div class="col-md-3">
                                <label for="fechaNominaHorarioEstablecido" class="control-label">Fecha de Nómina:</label>
                                <select name="fechaNominaHorarioEstablecido" id="fechaNominaHorarioEstablecido" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                  <?php
                                    foreach ($fechaNominaHE as $key => $datos) {
                                      # code...
                                      $keyArray = array_search($datos["fecha_nomina"],array_column($fullFechaNomina, 'FECHNOMI'));
                                      $fecha = strtotime($datos["fecha_nomina"]);
                                      echo "<option value='".$datos["fecha_nomina"]."'>Nom: ".$fullFechaNomina[$keyArray]['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                    }
                                  ?>
                                </select>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12 table-responsive">
                                <table id="tblHorarios" class="table table-hover table-striped table-bordered nowrap">
                                  <thead>
                                    <th>Nombre</th>
                                    <th>Horarios</th>
                                    <th>Acciones</th>
                                  </thead>
                                  <tbody class="jumbotron">
                                    
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="box-footer">
                          <div class="pull-right">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>

                  <?php if($booleanGrupo):?>
                  <div class="tab-pane <?php echo $classGrupo;?>" id="registro-grupos" style="position: relative; height: auto;">
                    <hr>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="box box-shadow">
                          <div class="box-header">
                            <h3 class="box-title">Alta de Grupo</h3>
                          </div>
                          <div class="box-body">
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label class="control-label">Empresa:</label>
                                    <select name="empresaGrupo" id="empresaGrupo" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                      
                                      <?php if(!empty($permisos['toluca_departamento'])){ ?>
                                      <option class="text-center" selected>TOLUCA</option>
                                      <?php } ?>
                                     
                                      <?php if(!empty($permisos['bosques_departamento'])){ ?>
                                      <option data-divider="true"></option>
                                      <option class="text-center" >BOSQUES</option>
                                      <?php } ?>
                                      
                                      <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                                      <option data-divider="true"></option>
                                      <option class="text-center" >COHISA</option>
                                      <?php } ?>

                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <label for="fechaNominaGrupo" class="control-label">Fecha de Nómina:</label>
                                  <select name="fechaNominaGrupo" id="fechaNominaGrupo" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                    <?php
                                      foreach ($fechaNomina as $key => $datos) {
                                        # code...
                                        $fecha = strtotime($datos["FECHNOMI"]);
                                        echo "<option value='".$datos["FECHNOMI"]."'>Nom: ".$datos['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                      }
                                    ?>
                                  </select>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">Nombre de Grupo:</label>
                                    <input type="text" id="nombreGrupo" class="form-control input-sm" placeholder="Nombre de grupo">
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12 table-responsive">
                                  <table id="tblPersonal" class="table table-hover table-striped table-bordered nowrap">
                                    <thead>
                                      <th></th>
                                      <th>Departamento</th>
                                      <th>Clave Trabajador</th>
                                      <th>Nombre Trabajador</th>
                                    </thead>
                                    <tbody class="jumbotron">
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="box-footer">
                            <div class="pull-right">
                              <button class="btn btn-sm btn-primary" id="btnCrearGrupo" disabled>
                                Crear
                              </button>
                              <button class="btn btn-sm btn-default " id="btnCancelarGrupo">
                                Cancelar
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="box box-shadow">
                          <div class="box-header">
                            <h3 class="box-title">Grupos establecidos</h3>
                          </div>
                          <div class="box-body">
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-3">
                                  <div class="form-group">
                                    <label class="control-label">Empresa:</label>
                                    <select name="empresaGrupo" id="empresaGrupoEstablecido" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                      
                                      <?php if(!empty($permisos['toluca_departamento'])){ ?>
                                      <option class="text-center" selected>TOLUCA</option>
                                      <?php } ?>
                                     
                                      <?php if(!empty($permisos['bosques_departamento'])){ ?>
                                      <option data-divider="true"></option>
                                      <option class="text-center" >BOSQUES</option>
                                      <?php } ?>
                                      
                                      <?php if(!empty($permisos['cohisa_departamento'])){ ?>
                                      <option data-divider="true"></option>
                                      <option class="text-center" >COHISA</option>
                                      <?php } ?>

                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-3">
                                  <label for="fechaNominaGrupoEstablecido" class="control-label">Fecha de Nómina:</label>
                                  <select name="fechaNominaGrupoEstablecido" id="fechaNominaGrupoEstablecido" class="selectpicker form-control input-sm" data-style="btn-info" required>
                                    <?php
                                      foreach ($fechaNominaGE as $key => $datos) {
                                        $keyArray = array_search($datos["fecha_nomina"],array_column($fullFechaNomina, 'FECHNOMI'));
                                        $fecha = strtotime($datos["fecha_nomina"]);
                                        echo "<option value='".$datos["fecha_nomina"]."'>Nom: ".$fullFechaNomina[$keyArray]['NUM_NOMINA']." ".date("d-m-Y",$fecha)."</option>";
                                      }
                                    ?>
                                  </select>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-md-12 table-responsive">
                                  <table id="tblGrupos" class="table table-hover table-striped table-bordered nowrap">
                                    <thead>
                                      <th>#</th>
                                      <th>Nombre</th>
                                      <th>Acciones</th>
                                    </thead>
                                    <tbody class="jumbotron">
                                      
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="box-footer">
                            <div class="pull-right">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </section> -->






            </div>
        </div>
        
    </div>
</div>
<!--**********************************
    Content body end
***********************************-->
<!-- footer -->
<?php include(__DIR__.'/foot.php');  ?>
<script src="<?php echo $rutaIndexArchivos; ?>/public/js/modulos/index_horario.js"></script>
<!-- footer -->