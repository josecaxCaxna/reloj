<?php
/**
* Pagina de login
*/
class login_view
{

    private $model;

    private $controller;

    function __construct($controller, $model)
    {
        $this->controller = $controller;
        $this->model = $model;
    }

    public function index()
    {
        return $this->controller->load();
    }

    public function access()
    {
        return $this->controller->access();
    }
    
    public function logout()
    {
        return $this->controller->logout();
    }

}
?>
<?php
$ruta = explode('/',$_SERVER['PHP_SELF']);
$rutaIndexArchivos = "http://".$_SERVER['SERVER_NAME']."/".$ruta[1]."";
?>
<!DOCTYPE html>
<html lang="en" class="h-100">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <title>Iniciar sesión </title>
        <link rel="icon" type="image/png" sizes="16x16" href="public/images/favicon.png">
        <link href="public/css/style.css" rel="stylesheet">
    </head>
    <body class="h-100">
        <div class="authincation h-100">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-md-6">
                        <div class="authincation-content">
                            <div class="row no-gutters">
                                <div class="col-xl-12">
                                    <div class="auth-form">
                                        <h3 class="text-center mb-4">Iniciar sesión</h3>
                                        <img src="" alt="" sizes="" srcset="">
                                        <div class="text-center">
                                            <img class="brand-title" src="<?php echo $rutaIndexArchivos; ?>/public/images/logo.jpeg" width="100px">
                                        </div>
                                        <form action="login/access" method="post">
                                            <div class="form-group">
                                                <label><strong>Usuario</strong></label>
                                                <input type="text" class="form-control" id="user" name="user">
                                            </div>
                                            <div class="form-group">
                                                <label><strong>Contraseña</strong></label>
                                                <input type="password" class="form-control" id="password" name="password">
                                            </div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
                                            </div>
                                            <? 
                                            if (isset($_SESSION['message']) && $_SESSION['message'] != '') {
                                            ?>                                            
                                            <div class="alert alert-danger solid alert-dismissible fade show mt-3">
                                                <button type="button" class="close h-100" data-dismiss="alert" aria-label="Close"><span><i class="mdi mdi-close"></i></span>
                                                </button>
                                                <strong>Error!</strong> <? echo $_SESSION['message']; ?>
                                            </div>                                            
                                            <? } ?>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="vendor/global/global.min.js"></script>
        <script src="vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="public/js/custom.min.js"></script>
        <script src="public/js/dlabnav-init.js"></script>
    </body>
</html>