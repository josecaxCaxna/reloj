<?php
    require_once(__DIR__.'/../models/index_model.php');
    /**
    * The home page controller
    */
    class index_controller
    {
        function __construct()
        {
            $this->model = new index_model();
        }

        public function dashboard()
        {
            return $this->model->showDashboard();
        }
        
        public function cargarEmpresas()
        {
            return $this->model->cargarEmpresas();
        }

    }
    ?>