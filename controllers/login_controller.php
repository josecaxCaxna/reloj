<?php

    /**
    * The home page controller
    */
    class login_controller
    {
        private $model;

        function __construct($model)
        {
            $this->model = $model;
        }

        public function load()
        {
            return $this->model->index();
        }
        
        public function access()
        {
            return $this->model->access();
        }
        
        public function logout()
        {
            return $this->model->logout();
        }

    }
?>