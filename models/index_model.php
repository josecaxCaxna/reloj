<?php
    require_once(__DIR__.'/../config/dbConfig.php');
    /**
    * The home page model
    */
    class index_model
    {
        protected $connection;
        function __construct()
        {
            $db = new dbConnection(); 
            $this->connection = $db->db();
        }

        public function executeQuery($query){
            $resultado = $this->connection->query($query);
            if($resultado){
                $nFilas = $resultado->num_rows;
                if($nFilas > 0){
                    while($fila = $resultado->fetch_assoc())
                        $dataOut[] = $fila;
                    return $dataOut;
                }else
                    return ["data"=>"empty"];
            }else
                return $this->connection->error;
        }

        public function cargarPermisos(){
            
        }
        
        public function cargarEmpresas(){
            $sql = "SELECT * FROM fp_empresa";
            $resultado = $this->executeQuery($sql);            
            if ($resultado) {
                return $resultado;
            }
        }


    }
?>